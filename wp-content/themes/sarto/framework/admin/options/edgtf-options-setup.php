<?php

if ( ! function_exists( 'sarto_edge_admin_map_init' ) ) {
	function sarto_edge_admin_map_init() {
		do_action( 'sarto_edge_before_options_map' );
		
		require_once EDGE_FRAMEWORK_ROOT_DIR . '/admin/options/fonts/map.php';
		require_once EDGE_FRAMEWORK_ROOT_DIR . '/admin/options/general/map.php';
		require_once EDGE_FRAMEWORK_ROOT_DIR . '/admin/options/page/map.php';
		require_once EDGE_FRAMEWORK_ROOT_DIR . '/admin/options/social/map.php';
		require_once EDGE_FRAMEWORK_ROOT_DIR . '/admin/options/reset/map.php';
		
		do_action( 'sarto_edge_options_map' );
		
		do_action( 'sarto_edge_after_options_map' );
	}
	
	add_action( 'after_setup_theme', 'sarto_edge_admin_map_init', 1 );
}