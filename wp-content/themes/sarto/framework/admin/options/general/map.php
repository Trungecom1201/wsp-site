<?php

if ( ! function_exists( 'sarto_edge_general_options_map' ) ) {
	/**
	 * General options page
	 */
	function sarto_edge_general_options_map() {
		
		sarto_edge_add_admin_page(
			array(
				'slug'  => '',
				'title' => esc_html__( 'General', 'sarto' ),
				'icon'  => 'fa fa-institution'
			)
		);
		
		$panel_design_style = sarto_edge_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_design_style',
				'title' => esc_html__( 'Design Style', 'sarto' )
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'google_fonts',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Google Font Family', 'sarto' ),
				'description'   => esc_html__( 'Choose a default Google font for your site', 'sarto' ),
				'parent'        => $panel_design_style
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'additional_google_fonts',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Additional Google Fonts', 'sarto' ),
				'parent'        => $panel_design_style
			)
		);
		
		$additional_google_fonts_container = sarto_edge_add_admin_container(
			array(
				'parent'          => $panel_design_style,
				'name'            => 'additional_google_fonts_container',
				'dependency' => array(
					'show' => array(
						'additional_google_fonts'  => 'yes'
					)
				)
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'additional_google_font1',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'sarto' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'sarto' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'additional_google_font2',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'sarto' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'sarto' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'additional_google_font3',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'sarto' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'sarto' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'additional_google_font4',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'sarto' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'sarto' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'additional_google_font5',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'sarto' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'sarto' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'google_font_weight',
				'type'          => 'checkboxgroup',
				'default_value' => '',
				'label'         => esc_html__( 'Google Fonts Style & Weight', 'sarto' ),
				'description'   => esc_html__( 'Choose a default Google font weights for your site. Impact on page load time', 'sarto' ),
				'parent'        => $panel_design_style,
				'options'       => array(
					'100'  => esc_html__( '100 Thin', 'sarto' ),
					'100i' => esc_html__( '100 Thin Italic', 'sarto' ),
					'200'  => esc_html__( '200 Extra-Light', 'sarto' ),
					'200i' => esc_html__( '200 Extra-Light Italic', 'sarto' ),
					'300'  => esc_html__( '300 Light', 'sarto' ),
					'300i' => esc_html__( '300 Light Italic', 'sarto' ),
					'400'  => esc_html__( '400 Regular', 'sarto' ),
					'400i' => esc_html__( '400 Regular Italic', 'sarto' ),
					'500'  => esc_html__( '500 Medium', 'sarto' ),
					'500i' => esc_html__( '500 Medium Italic', 'sarto' ),
					'600'  => esc_html__( '600 Semi-Bold', 'sarto' ),
					'600i' => esc_html__( '600 Semi-Bold Italic', 'sarto' ),
					'700'  => esc_html__( '700 Bold', 'sarto' ),
					'700i' => esc_html__( '700 Bold Italic', 'sarto' ),
					'800'  => esc_html__( '800 Extra-Bold', 'sarto' ),
					'800i' => esc_html__( '800 Extra-Bold Italic', 'sarto' ),
					'900'  => esc_html__( '900 Ultra-Bold', 'sarto' ),
					'900i' => esc_html__( '900 Ultra-Bold Italic', 'sarto' )
				)
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'google_font_subset',
				'type'          => 'checkboxgroup',
				'default_value' => '',
				'label'         => esc_html__( 'Google Fonts Subset', 'sarto' ),
				'description'   => esc_html__( 'Choose a default Google font subsets for your site', 'sarto' ),
				'parent'        => $panel_design_style,
				'options'       => array(
					'latin'        => esc_html__( 'Latin', 'sarto' ),
					'latin-ext'    => esc_html__( 'Latin Extended', 'sarto' ),
					'cyrillic'     => esc_html__( 'Cyrillic', 'sarto' ),
					'cyrillic-ext' => esc_html__( 'Cyrillic Extended', 'sarto' ),
					'greek'        => esc_html__( 'Greek', 'sarto' ),
					'greek-ext'    => esc_html__( 'Greek Extended', 'sarto' ),
					'vietnamese'   => esc_html__( 'Vietnamese', 'sarto' )
				)
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'        => 'first_color',
				'type'        => 'color',
				'label'       => esc_html__( 'First Main Color', 'sarto' ),
				'description' => esc_html__( 'Choose the most dominant theme color. Default color is #00bbb3', 'sarto' ),
				'parent'      => $panel_design_style
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'        => 'page_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Page Background Color', 'sarto' ),
				'description' => esc_html__( 'Choose the background color for page content. Default color is #ffffff', 'sarto' ),
				'parent'      => $panel_design_style
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'        => 'selection_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Text Selection Color', 'sarto' ),
				'description' => esc_html__( 'Choose the color users see when selecting text', 'sarto' ),
				'parent'      => $panel_design_style
			)
		);
		
		/***************** Passepartout Layout - begin **********************/
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'boxed',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Boxed Layout', 'sarto' ),
				'parent'        => $panel_design_style
			)
		);
		
			$boxed_container = sarto_edge_add_admin_container(
				array(
					'parent'          => $panel_design_style,
					'name'            => 'boxed_container',
					'dependency' => array(
						'show' => array(
							'boxed'  => 'yes'
						)
					)
				)
			);
		
				sarto_edge_add_admin_field(
					array(
						'name'        => 'page_background_color_in_box',
						'type'        => 'color',
						'label'       => esc_html__( 'Page Background Color', 'sarto' ),
						'description' => esc_html__( 'Choose the page background color outside box', 'sarto' ),
						'parent'      => $boxed_container
					)
				);
				
				sarto_edge_add_admin_field(
					array(
						'name'        => 'boxed_background_image',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'sarto' ),
						'description' => esc_html__( 'Choose an image to be displayed in background', 'sarto' ),
						'parent'      => $boxed_container
					)
				);
				
				sarto_edge_add_admin_field(
					array(
						'name'        => 'boxed_pattern_background_image',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Pattern', 'sarto' ),
						'description' => esc_html__( 'Choose an image to be used as background pattern', 'sarto' ),
						'parent'      => $boxed_container
					)
				);
				
				sarto_edge_add_admin_field(
					array(
						'name'          => 'boxed_background_image_attachment',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Background Image Attachment', 'sarto' ),
						'description'   => esc_html__( 'Choose background image attachment', 'sarto' ),
						'parent'        => $boxed_container,
						'options'       => array(
							''       => esc_html__( 'Default', 'sarto' ),
							'fixed'  => esc_html__( 'Fixed', 'sarto' ),
							'scroll' => esc_html__( 'Scroll', 'sarto' )
						)
					)
				);
		
		/***************** Boxed Layout - end **********************/
		
		/***************** Passepartout Layout - begin **********************/
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'paspartu',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Passepartout', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will display passepartout around site content', 'sarto' ),
				'parent'        => $panel_design_style
			)
		);
		
			$paspartu_container = sarto_edge_add_admin_container(
				array(
					'parent'          => $panel_design_style,
					'name'            => 'paspartu_container',
					'dependency' => array(
						'show' => array(
							'paspartu'  => 'yes'
						)
					)
				)
			);
		
				sarto_edge_add_admin_field(
					array(
						'name'        => 'paspartu_color',
						'type'        => 'color',
						'label'       => esc_html__( 'Passepartout Color', 'sarto' ),
						'description' => esc_html__( 'Choose passepartout color, default value is #ffffff', 'sarto' ),
						'parent'      => $paspartu_container
					)
				);
				
				sarto_edge_add_admin_field(
					array(
						'name'        => 'paspartu_width',
						'type'        => 'text',
						'label'       => esc_html__( 'Passepartout Size', 'sarto' ),
						'description' => esc_html__( 'Enter size amount for passepartout', 'sarto' ),
						'parent'      => $paspartu_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
		
				sarto_edge_add_admin_field(
					array(
						'name'        => 'paspartu_responsive_width',
						'type'        => 'text',
						'label'       => esc_html__( 'Responsive Passepartout Size', 'sarto' ),
						'description' => esc_html__( 'Enter size amount for passepartout for smaller screens (tablets and mobiles view)', 'sarto' ),
						'parent'      => $paspartu_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
				
				sarto_edge_add_admin_field(
					array(
						'parent'        => $paspartu_container,
						'type'          => 'yesno',
						'default_value' => 'no',
						'name'          => 'disable_top_paspartu',
						'label'         => esc_html__( 'Disable Top Passepartout', 'sarto' )
					)
				);
		
				sarto_edge_add_admin_field(
					array(
						'parent'        => $paspartu_container,
						'type'          => 'yesno',
						'default_value' => 'no',
						'name'          => 'enable_fixed_paspartu',
						'label'         => esc_html__( 'Enable Fixed Passepartout', 'sarto' ),
						'description' => esc_html__( 'Enabling this option will set fixed passepartout for your screens', 'sarto' )
					)
				);
		
		/***************** Passepartout Layout - end **********************/
		
		/***************** Content Layout - begin **********************/
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'initial_content_width',
				'type'          => 'select',
				'default_value' => 'edgtf-grid-1300',
				'label'         => esc_html__( 'Initial Width of Content', 'sarto' ),
				'description'   => esc_html__( 'Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'sarto' ),
				'parent'        => $panel_design_style,
				'options'       => array(
					'edgtf-grid-1100' => esc_html__( '1100px', 'sarto' ),
					'edgtf-grid-1300' => esc_html__( '1300px - default', 'sarto' ),
					'edgtf-grid-1200' => esc_html__( '1200px', 'sarto' ),
					'edgtf-grid-1000' => esc_html__( '1000px', 'sarto' ),
					'edgtf-grid-800'  => esc_html__( '800px', 'sarto' )
				)
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'preload_pattern_image',
				'type'          => 'image',
				'label'         => esc_html__( 'Preload Pattern Image', 'sarto' ),
				'description'   => esc_html__( 'Choose preload pattern image to be displayed until images are loaded', 'sarto' ),
				'parent'        => $panel_design_style
			)
		);
		
		/***************** Content Layout - end **********************/
		
		$panel_settings = sarto_edge_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_settings',
				'title' => esc_html__( 'Settings', 'sarto' )
			)
		);
		
		/***************** Smooth Scroll Layout - begin **********************/
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'page_smooth_scroll',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Smooth Scroll', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth scrolling effect on every page (except on Mac and touch devices)', 'sarto' ),
				'parent'        => $panel_settings
			)
		);
		
		/***************** Smooth Scroll Layout - end **********************/
		
		/***************** Smooth Page Transitions Layout - begin **********************/
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'smooth_page_transitions',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Smooth Page Transitions', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links', 'sarto' ),
				'parent'        => $panel_settings
			)
		);
		
			$page_transitions_container = sarto_edge_add_admin_container(
				array(
					'parent'          => $panel_settings,
					'name'            => 'page_transitions_container',
					'dependency' => array(
						'show' => array(
							'smooth_page_transitions'  => 'yes'
						)
					)
				)
			);
		
				sarto_edge_add_admin_field(
					array(
						'name'          => 'page_transition_preloader',
						'type'          => 'yesno',
						'default_value' => 'no',
						'label'         => esc_html__( 'Enable Preloading Animation', 'sarto' ),
						'description'   => esc_html__( 'Enabling this option will display an animated preloader while the page content is loading', 'sarto' ),
						'parent'        => $page_transitions_container
					)
				);
				
				$page_transition_preloader_container = sarto_edge_add_admin_container(
					array(
						'parent'          => $page_transitions_container,
						'name'            => 'page_transition_preloader_container',
						'dependency' => array(
							'show' => array(
								'page_transition_preloader'  => 'yes'
							)
						)
					)
				);
		
		
					sarto_edge_add_admin_field(
						array(
							'name'   => 'smooth_pt_bgnd_color',
							'type'   => 'color',
							'label'  => esc_html__( 'Page Loader Background Color', 'sarto' ),
							'parent' => $page_transition_preloader_container
						)
					);
					
					$group_pt_spinner_animation = sarto_edge_add_admin_group(
						array(
							'name'        => 'group_pt_spinner_animation',
							'title'       => esc_html__( 'Loader Style', 'sarto' ),
							'description' => esc_html__( 'Define styles for loader spinner animation', 'sarto' ),
							'parent'      => $page_transition_preloader_container
						)
					);
					
					$row_pt_spinner_animation = sarto_edge_add_admin_row(
						array(
							'name'   => 'row_pt_spinner_animation',
							'parent' => $group_pt_spinner_animation
						)
					);
					
					sarto_edge_add_admin_field(
						array(
							'type'          => 'selectsimple',
							'name'          => 'smooth_pt_spinner_type',
							'default_value' => '',
							'label'         => esc_html__( 'Spinner Type', 'sarto' ),
							'parent'        => $row_pt_spinner_animation,
							'options'       => array(
								'text_loader'       	=> esc_html__( 'Text Loader', 'sarto' ),
								'rotate_circles'        => esc_html__( 'Rotate Circles', 'sarto' ),
								'pulse'                 => esc_html__( 'Pulse', 'sarto' ),
								'double_pulse'          => esc_html__( 'Double Pulse', 'sarto' ),
								'cube'                  => esc_html__( 'Cube', 'sarto' ),
								'rotating_cubes'        => esc_html__( 'Rotating Cubes', 'sarto' ),
								'stripes'               => esc_html__( 'Stripes', 'sarto' ),
								'wave'                  => esc_html__( 'Wave', 'sarto' ),
								'two_rotating_circles'  => esc_html__( '2 Rotating Circles', 'sarto' ),
								'five_rotating_circles' => esc_html__( '5 Rotating Circles', 'sarto' ),
								'atom'                  => esc_html__( 'Atom', 'sarto' ),
								'clock'                 => esc_html__( 'Clock', 'sarto' ),
								'mitosis'               => esc_html__( 'Mitosis', 'sarto' ),
								'lines'                 => esc_html__( 'Lines', 'sarto' ),
								'fussion'               => esc_html__( 'Fussion', 'sarto' ),
								'wave_circles'          => esc_html__( 'Wave Circles', 'sarto' ),
								'pulse_circles'         => esc_html__( 'Pulse Circles', 'sarto' )
							),
							'args'          => array(
							    "dependence"             => true,
							    'show'        => array(
							        "text_loader"         	=> "#edgtf_text_loader_title_container",
							        "rotate_circles"        => "",
							        "pulse"                 => "",
							        "double_pulse"          => "",
							        "cube"                  => "",
							        "rotating_cubes"        => "",
							        "stripes"               => "",
							        "wave"                  => "",
							        "two_rotating_circles"  => "",
							        "five_rotating_circles" => "",
							        "atom"                  => "",
							        "clock"                 => "",
							        "mitosis"               => "",
							        "lines"                 => "",
							        "fussion"               => "",
							        "wave_circles"          => "",
							        "pulse_circles"         => ""
							    ),
							    'hide'        => array(
							        "text_loader"         	=> "",
							        ""                      => "#edgtf_text_loader_title_container",
							        "rotate_circles"        => "#edgtf_text_loader_title_container",
							        "pulse"                 => "#edgtf_text_loader_title_container",
							        "double_pulse"          => "#edgtf_text_loader_title_container",
							        "cube"                  => "#edgtf_text_loader_title_container",
							        "rotating_cubes"        => "#edgtf_text_loader_title_container",
							        "stripes"               => "#edgtf_text_loader_title_container",
							        "wave"                  => "#edgtf_text_loader_title_container",
							        "two_rotating_circles"  => "#edgtf_text_loader_title_container",
							        "five_rotating_circles" => "#edgtf_text_loader_title_container",
							        "atom"                  => "#edgtf_text_loader_title_container",
							        "clock"                 => "#edgtf_text_loader_title_container",
							        "mitosis"               => "#edgtf_text_loader_title_container",
							        "lines"                 => "#edgtf_text_loader_title_container",
							        "fussion"               => "#edgtf_text_loader_title_container",
							        "wave_circles"          => "#edgtf_text_loader_title_container",
							        "pulse_circles"         => "#edgtf_text_loader_title_container"
							    )
							)
						)
					);
					
					sarto_edge_add_admin_field(
						array(
							'type'          => 'colorsimple',
							'name'          => 'smooth_pt_spinner_color',
							'default_value' => '',
							'label'         => esc_html__( 'Spinner Color', 'sarto' ),
							'parent'        => $row_pt_spinner_animation
						)
					);

					$text_loader_title_container = sarto_edge_add_admin_container(
					    array(
					        'name'            => 'text_loader_title_container',
					        'hidden_property' => 'smooth_pt_spinner_type',
					        'hidden_value'    => '',
					        'hidden_values'   =>array(
					            "",
					            "rotate_circles",
					            "pulse",
					            "double_pulse",
					            "cube",
					            "rotating_cubes",
					            "stripes",
					            "wave",
					            "two_rotating_circles",
					            "five_rotating_circles",
					            "atom",
					            "clock",
					            "mitosis",
					            "lines",
					            "fussion",
					            "wave_circles",
					            "pulse_circles"
					        ),
							'parent'        => $page_transitions_container
					    )
					);

					sarto_edge_add_admin_field(
					    array(
					        'type'          => 'text',
					        'name'          => 'sarto_loader_title',
					        'default_value' => 'Sarto',
					        'label'         => esc_html__('Loader Title', 'sarto'),
							'description'   => esc_html__( 'Enter the text that will be used for Text Loader.', 'sarto' ),
					        'args' => array(
					            'col_width' => 2,
					        ),
					        'parent'        => $text_loader_title_container,
					    )
					);
					
					sarto_edge_add_admin_field(
						array(
							'name'          => 'page_transition_fadeout',
							'type'          => 'yesno',
							'default_value' => 'no',
							'label'         => esc_html__( 'Enable Fade Out Animation', 'sarto' ),
							'description'   => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'sarto' ),
							'parent'        => $page_transitions_container
						)
					);
		
		/***************** Smooth Page Transitions Layout - end **********************/
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'show_back_button',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show "Back To Top Button"', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will display a Back to Top button on every page', 'sarto' ),
				'parent'        => $panel_settings
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'responsiveness',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Responsiveness', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will make all pages responsive', 'sarto' ),
				'parent'        => $panel_settings
			)
		);
		
		$panel_custom_code = sarto_edge_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_custom_code',
				'title' => esc_html__( 'Custom Code', 'sarto' )
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'        => 'custom_js',
				'type'        => 'textarea',
				'label'       => esc_html__( 'Custom JS', 'sarto' ),
				'description' => esc_html__( 'Enter your custom Javascript here', 'sarto' ),
				'parent'      => $panel_custom_code
			)
		);
		
		$panel_google_api = sarto_edge_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_google_api',
				'title' => esc_html__( 'Google API', 'sarto' )
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'        => 'google_maps_api_key',
				'type'        => 'text',
				'label'       => esc_html__( 'Google Maps Api Key', 'sarto' ),
				'description' => esc_html__( 'Insert your Google Maps API key here. For instructions on how to create a Google Maps API key, please refer to our to our documentation.', 'sarto' ),
				'parent'      => $panel_google_api
			)
		);
	}
	
	add_action( 'sarto_edge_options_map', 'sarto_edge_general_options_map', 1 );
}

if ( ! function_exists( 'sarto_edge_page_general_style' ) ) {
	/**
	 * Function that prints page general inline styles
	 */
	function sarto_edge_page_general_style( $style ) {
		$current_style = '';
		$page_id       = sarto_edge_get_page_id();
		$class_prefix  = sarto_edge_get_unique_page_class( $page_id );
		
		$boxed_background_style = array();
		
		$boxed_page_background_color = sarto_edge_get_meta_field_intersect( 'page_background_color_in_box', $page_id );
		if ( ! empty( $boxed_page_background_color ) ) {
			$boxed_background_style['background-color'] = $boxed_page_background_color;
		}
		
		$boxed_page_background_image = sarto_edge_get_meta_field_intersect( 'boxed_background_image', $page_id );
		if ( ! empty( $boxed_page_background_image ) ) {
			$boxed_background_style['background-image']    = 'url(' . esc_url( $boxed_page_background_image ) . ')';
			$boxed_background_style['background-position'] = 'center 0px';
			$boxed_background_style['background-repeat']   = 'no-repeat';
		}
		
		$boxed_page_background_pattern_image = sarto_edge_get_meta_field_intersect( 'boxed_pattern_background_image', $page_id );
		if ( ! empty( $boxed_page_background_pattern_image ) ) {
			$boxed_background_style['background-image']    = 'url(' . esc_url( $boxed_page_background_pattern_image ) . ')';
			$boxed_background_style['background-position'] = '0px 0px';
			$boxed_background_style['background-repeat']   = 'repeat';
		}
		
		$boxed_page_background_attachment = sarto_edge_get_meta_field_intersect( 'boxed_background_image_attachment', $page_id );
		if ( ! empty( $boxed_page_background_attachment ) ) {
			$boxed_background_style['background-attachment'] = $boxed_page_background_attachment;
		}
		
		$boxed_background_selector = $class_prefix . '.edgtf-boxed .edgtf-wrapper';
		
		if ( ! empty( $boxed_background_style ) ) {
			$current_style .= sarto_edge_dynamic_css( $boxed_background_selector, $boxed_background_style );
		}
		
		$paspartu_style     = array();
		$paspartu_res_style = array();
		$paspartu_res_start = '@media only screen and (max-width: 1024px) {';
		$paspartu_res_end   = '}';
		
		$paspartu_header_selector                = array(
			'.edgtf-paspartu-enabled .edgtf-page-header .edgtf-fixed-wrapper.fixed',
			'.edgtf-paspartu-enabled .edgtf-sticky-header',
			'.edgtf-paspartu-enabled .edgtf-mobile-header.mobile-header-appear .edgtf-mobile-header-inner'
		);
		$paspartu_header_appear_selector         = array(
			'.edgtf-paspartu-enabled.edgtf-fixed-paspartu-enabled .edgtf-page-header .edgtf-fixed-wrapper.fixed',
			'.edgtf-paspartu-enabled.edgtf-fixed-paspartu-enabled .edgtf-sticky-header.header-appear',
			'.edgtf-paspartu-enabled.edgtf-fixed-paspartu-enabled .edgtf-mobile-header.mobile-header-appear .edgtf-mobile-header-inner'
		);
		
		$paspartu_header_style                   = array();
		$paspartu_header_appear_style            = array();
		$paspartu_header_responsive_style        = array();
		$paspartu_header_appear_responsive_style = array();
		
		$paspartu_color = sarto_edge_get_meta_field_intersect( 'paspartu_color', $page_id );
		if ( ! empty( $paspartu_color ) ) {
			$paspartu_style['background-color'] = $paspartu_color;
		}
		
		$paspartu_width = sarto_edge_get_meta_field_intersect( 'paspartu_width', $page_id );
		if ( $paspartu_width !== '' ) {
			if ( sarto_edge_string_ends_with( $paspartu_width, '%' ) || sarto_edge_string_ends_with( $paspartu_width, 'px' ) ) {
				$paspartu_style['padding'] = $paspartu_width;
				
				$paspartu_clean_width      = sarto_edge_string_ends_with( $paspartu_width, '%' ) ? sarto_edge_filter_suffix( $paspartu_width, '%' ) : sarto_edge_filter_suffix( $paspartu_width, 'px' );
				$paspartu_clean_width_mark = sarto_edge_string_ends_with( $paspartu_width, '%' ) ? '%' : 'px';
				
				$paspartu_header_style['left']              = $paspartu_width;
				$paspartu_header_style['width']             = 'calc(100% - ' . ( 2 * $paspartu_clean_width ) . $paspartu_clean_width_mark . ')';
				$paspartu_header_appear_style['margin-top'] = $paspartu_width;
			} else {
				$paspartu_style['padding'] = $paspartu_width . 'px';
				
				$paspartu_header_style['left']              = $paspartu_width . 'px';
				$paspartu_header_style['width']             = 'calc(100% - ' . ( 2 * $paspartu_width ) . 'px)';
				$paspartu_header_appear_style['margin-top'] = $paspartu_width . 'px';
			}
		}
		
		$paspartu_selector = $class_prefix . '.edgtf-paspartu-enabled .edgtf-wrapper';
		
		if ( ! empty( $paspartu_style ) ) {
			$current_style .= sarto_edge_dynamic_css( $paspartu_selector, $paspartu_style );
		}
		
		if ( ! empty( $paspartu_header_style ) ) {
			$current_style .= sarto_edge_dynamic_css( $paspartu_header_selector, $paspartu_header_style );
			$current_style .= sarto_edge_dynamic_css( $paspartu_header_appear_selector, $paspartu_header_appear_style );
		}
		
		$paspartu_responsive_width = sarto_edge_get_meta_field_intersect( 'paspartu_responsive_width', $page_id );
		if ( $paspartu_responsive_width !== '' ) {
			if ( sarto_edge_string_ends_with( $paspartu_responsive_width, '%' ) || sarto_edge_string_ends_with( $paspartu_responsive_width, 'px' ) ) {
				$paspartu_res_style['padding'] = $paspartu_responsive_width;
				
				$paspartu_clean_width      = sarto_edge_string_ends_with( $paspartu_responsive_width, '%' ) ? sarto_edge_filter_suffix( $paspartu_responsive_width, '%' ) : sarto_edge_filter_suffix( $paspartu_responsive_width, 'px' );
				$paspartu_clean_width_mark = sarto_edge_string_ends_with( $paspartu_responsive_width, '%' ) ? '%' : 'px';
				
				$paspartu_header_responsive_style['left']              = $paspartu_responsive_width;
				$paspartu_header_responsive_style['width']             = 'calc(100% - ' . ( 2 * $paspartu_clean_width ) . $paspartu_clean_width_mark . ')';
				$paspartu_header_appear_responsive_style['margin-top'] = $paspartu_responsive_width;
			} else {
				$paspartu_res_style['padding'] = $paspartu_responsive_width . 'px';
				
				$paspartu_header_responsive_style['left']              = $paspartu_responsive_width . 'px';
				$paspartu_header_responsive_style['width']             = 'calc(100% - ' . ( 2 * $paspartu_responsive_width ) . 'px)';
				$paspartu_header_appear_responsive_style['margin-top'] = $paspartu_responsive_width . 'px';
			}
		}
		
		if ( ! empty( $paspartu_res_style ) ) {
			$current_style .= $paspartu_res_start . sarto_edge_dynamic_css( $paspartu_selector, $paspartu_res_style ) . $paspartu_res_end;
		}
		
		if ( ! empty( $paspartu_header_responsive_style ) ) {
			$current_style .= $paspartu_res_start . sarto_edge_dynamic_css( $paspartu_header_selector, $paspartu_header_responsive_style ) . $paspartu_res_end;
			$current_style .= $paspartu_res_start . sarto_edge_dynamic_css( $paspartu_header_appear_selector, $paspartu_header_appear_responsive_style ) . $paspartu_res_end;
		}
		
		$current_style = $current_style . $style;
		
		return $current_style;
	}
	
	add_filter( 'sarto_edge_add_page_custom_style', 'sarto_edge_page_general_style' );
}