<?php

if ( ! function_exists( 'sarto_edge_reset_options_map' ) ) {
	/**
	 * Reset options panel
	 */
	function sarto_edge_reset_options_map() {
		
		sarto_edge_add_admin_page(
			array(
				'slug'  => '_reset_page',
				'title' => esc_html__( 'Reset', 'sarto' ),
				'icon'  => 'fa fa-retweet'
			)
		);
		
		$panel_reset = sarto_edge_add_admin_panel(
			array(
				'page'  => '_reset_page',
				'name'  => 'panel_reset',
				'title' => esc_html__( 'Reset', 'sarto' )
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'reset_to_defaults',
				'default_value' => 'no',
				'label'         => esc_html__( 'Reset to Defaults', 'sarto' ),
				'description'   => esc_html__( 'This option will reset all Select Options values to defaults', 'sarto' ),
				'parent'        => $panel_reset
			)
		);
	}
	
	add_action( 'sarto_edge_options_map', 'sarto_edge_reset_options_map', 100 );
}