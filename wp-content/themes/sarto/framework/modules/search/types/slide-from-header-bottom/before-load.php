<?php

if ( ! function_exists( 'sarto_edge_set_search_slide_from_hb_global_option' ) ) {
    /**
     * This function set search type value for search options map
     */
    function sarto_edge_set_search_slide_from_hb_global_option( $search_type_options ) {
        $search_type_options['slide-from-header-bottom'] = esc_html__( 'Slide From Header Bottom', 'sarto' );

        return $search_type_options;
    }

    add_filter( 'sarto_edge_search_type_global_option', 'sarto_edge_set_search_slide_from_hb_global_option' );
}