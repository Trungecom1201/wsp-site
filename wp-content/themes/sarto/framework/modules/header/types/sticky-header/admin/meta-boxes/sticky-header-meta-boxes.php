<?php

if ( ! function_exists( 'sarto_edge_sticky_header_meta_boxes_options_map' ) ) {
	function sarto_edge_sticky_header_meta_boxes_options_map( $header_meta_box ) {
		
		$sticky_amount_container = sarto_edge_add_admin_container(
			array(
				'parent'          => $header_meta_box,
				'name'            => 'sticky_amount_container_meta_container',
				'dependency' => array(
					'hide' => array(
						'edgtf_header_behaviour_meta'  => array( '', 'no-behavior','fixed-on-scroll','sticky-header-on-scroll-up' )
					)
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_scroll_amount_for_sticky_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Scroll Amount for Sticky Header Appearance', 'sarto' ),
				'description' => esc_html__( 'Define scroll amount for sticky header appearance', 'sarto' ),
				'parent'      => $sticky_amount_container,
				'args'        => array(
					'col_width' => 2,
					'suffix'    => 'px'
				)
			)
		);
		
		$sarto_custom_sidebars = sarto_edge_get_custom_sidebars();
		if ( count( $sarto_custom_sidebars ) > 0 ) {
			sarto_edge_create_meta_box_field(
				array(
					'name'        => 'edgtf_custom_sticky_menu_area_sidebar_meta',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Choose Custom Widget Area In Sticky Header Menu Area', 'sarto' ),
					'description' => esc_html__( 'Choose custom widget area to display in sticky header menu area"', 'sarto' ),
					'parent'      => $header_meta_box,
					'options'     => $sarto_custom_sidebars,
					'dependency' => array(
						'show' => array(
							'edgtf_header_behaviour_meta' => array( 'sticky-header-on-scroll-up', 'sticky-header-on-scroll-down-up' )
						)
					)
				)
			);
		}
	}
	
	add_action( 'sarto_edge_additional_header_area_meta_boxes_map', 'sarto_edge_sticky_header_meta_boxes_options_map', 8, 1 );
}