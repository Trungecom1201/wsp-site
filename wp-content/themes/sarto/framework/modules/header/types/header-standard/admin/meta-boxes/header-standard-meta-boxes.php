<?php

if ( ! function_exists( 'sarto_edge_get_hide_dep_for_header_standard_meta_boxes' ) ) {
	function sarto_edge_get_hide_dep_for_header_standard_meta_boxes() {
		$hide_dep_options = apply_filters( 'sarto_edge_header_standard_hide_meta_boxes', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'sarto_edge_header_standard_meta_map' ) ) {
	function sarto_edge_header_standard_meta_map( $parent ) {
		$hide_dep_options = sarto_edge_get_hide_dep_for_header_standard_meta_boxes();
		
		sarto_edge_create_meta_box_field(
			array(
				'parent'          => $parent,
				'type'            => 'select',
				'name'            => 'edgtf_set_menu_area_position_meta',
				'default_value'   => '',
				'label'           => esc_html__( 'Choose Menu Area Position', 'sarto' ),
				'description'     => esc_html__( 'Select menu area position in your header', 'sarto' ),
				'options'         => array(
					''       => esc_html__( 'Default', 'sarto' ),
					'left'   => esc_html__( 'Left', 'sarto' ),
					'right'  => esc_html__( 'Right', 'sarto' ),
					'center' => esc_html__( 'Center', 'sarto' )
				),
				'dependency' => array(
					'hide' => array(
						'edgtf_header_type_meta'  => $hide_dep_options
					)
				)
			)
		);
	}
	
	add_action( 'sarto_edge_additional_header_area_meta_boxes_map', 'sarto_edge_header_standard_meta_map' );
}