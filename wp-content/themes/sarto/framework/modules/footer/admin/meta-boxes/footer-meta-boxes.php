<?php

if ( ! function_exists( 'sarto_edge_map_footer_meta' ) ) {
	function sarto_edge_map_footer_meta() {
		
		$footer_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => apply_filters( 'sarto_edge_set_scope_for_meta_boxes', array( 'page', 'post' ), 'footer_meta' ),
				'title' => esc_html__( 'Footer', 'sarto' ),
				'name'  => 'footer_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_disable_footer_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Disable Footer for this Page', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will hide footer on this page', 'sarto' ),
				'options'       => sarto_edge_get_yes_no_select_array(),
				'parent'        => $footer_meta_box
			)
		);
		
		$show_footer_meta_container = sarto_edge_add_admin_container(
			array(
				'name'       => 'edgtf_show_footer_meta_container',
				'parent'     => $footer_meta_box,
				'dependency' => array(
					'hide' => array(
						'edgtf_disable_footer_meta' => 'yes'
					)
				)
			)
		);
		
			sarto_edge_create_meta_box_field(
				array(
					'name'          => 'edgtf_show_footer_top_meta',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__( 'Show Footer Top', 'sarto' ),
					'description'   => esc_html__( 'Enabling this option will show Footer Top area', 'sarto' ),
					'options'       => sarto_edge_get_yes_no_select_array(),
					'parent'        => $show_footer_meta_container
				)
			);
			
			sarto_edge_create_meta_box_field(
				array(
					'name'          => 'edgtf_show_footer_bottom_meta',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__( 'Show Footer Bottom', 'sarto' ),
					'description'   => esc_html__( 'Enabling this option will show Footer Bottom area', 'sarto' ),
					'options'       => sarto_edge_get_yes_no_select_array(),
					'parent'        => $show_footer_meta_container
				)
			);

        sarto_edge_create_meta_box_field(
            array(
                'name'          => 'edgtf_footer_in_grid_meta',
                'type'          => 'select',
                'default_value' => '',
                'label'         => esc_html__( 'Footer in Grid', 'sarto' ),
                'description'   => esc_html__( 'Enabling this option will place Footer content in grid', 'sarto' ),
                'options'       => sarto_edge_get_yes_no_select_array(),
                'dependency' => array(
                    'hide' => array(
                        'edgtf_show_footer_top_meta' => array('', 'no'),
                        'edgtf_show_footer_bottom_meta' => array('', 'no')
                    )
                ),
                'parent'        => $show_footer_meta_container
            )
        );

        sarto_edge_create_meta_box_field(
            array(
                'name'          => 'edgtf_uncovering_footer_meta',
                'type'          => 'select',
                'default_value' => '',
                'label'         => esc_html__( 'Uncovering Footer', 'sarto' ),
                'description'   => esc_html__( 'Enabling this option will make Footer gradually appear on scroll', 'sarto' ),
                'options'       => sarto_edge_get_yes_no_select_array(),
                'parent'        => $show_footer_meta_container,
            )
        );
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_footer_meta', 70 );
}