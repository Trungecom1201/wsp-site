<?php

if (!function_exists('sarto_edge_sidearea_options_map')) {
    function sarto_edge_sidearea_options_map() {

        sarto_edge_add_admin_page(
            array(
                'slug'  => '_side_area_page',
                'title' => esc_html__('Side Area', 'sarto'),
                'icon'  => 'fa fa-indent'
            )
        );

        $side_area_panel = sarto_edge_add_admin_panel(
            array(
                'title' => esc_html__('Side Area', 'sarto'),
                'name'  => 'side_area',
                'page'  => '_side_area_page'
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'        => $side_area_panel,
                'type'          => 'select',
                'name'          => 'side_area_type',
                'default_value' => 'side-menu-slide-from-right',
                'label'         => esc_html__('Side Area Type', 'sarto'),
                'description'   => esc_html__('Choose a type of Side Area', 'sarto'),
                'options'       => array(
                    'side-menu-slide-from-right'       => esc_html__('Slide from Right Over Content', 'sarto'),
                    'side-menu-slide-with-content'     => esc_html__('Slide from Right With Content', 'sarto'),
                    'side-area-uncovered-from-content' => esc_html__('Side Area Uncovered from Content', 'sarto'),
                ),
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'        => $side_area_panel,
                'type'          => 'text',
                'name'          => 'side_area_width',
                'default_value' => '',
                'label'         => esc_html__('Side Area Width', 'sarto'),
                'description'   => esc_html__('Enter a width for Side Area (px or %). Default width: 401px.', 'sarto'),
                'args'          => array(
                    'col_width' => 3,
                )
            )
        );

        $side_area_width_container = sarto_edge_add_admin_container(
            array(
                'parent'     => $side_area_panel,
                'name'       => 'side_area_width_container',
                'dependency' => array(
                    'show' => array(
                        'side_area_type' => 'side-menu-slide-from-right',
                    )
                )
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'        => $side_area_width_container,
                'type'          => 'color',
                'name'          => 'side_area_content_overlay_color',
                'default_value' => '',
                'label'         => esc_html__('Content Overlay Background Color', 'sarto'),
                'description'   => esc_html__('Choose a background color for a content overlay', 'sarto'),
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'        => $side_area_width_container,
                'type'          => 'text',
                'name'          => 'side_area_content_overlay_opacity',
                'default_value' => '',
                'label'         => esc_html__('Content Overlay Background Transparency', 'sarto'),
                'description'   => esc_html__('Choose a transparency for the content overlay background color (0 = fully transparent, 1 = opaque)', 'sarto'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'        => $side_area_panel,
                'type'          => 'select',
                'name'          => 'side_area_icon_source',
                'default_value' => 'icon_pack',
                'label'         => esc_html__('Select Side Area Icon Source', 'sarto'),
                'description'   => esc_html__('Choose whether you would like to use icons from an icon pack or SVG icons', 'sarto'),
                'options'       => sarto_edge_get_icon_sources_array()
            )
        );

        $side_area_icon_pack_container = sarto_edge_add_admin_container(
            array(
                'parent'     => $side_area_panel,
                'name'       => 'side_area_icon_pack_container',
                'dependency' => array(
                    'show' => array(
                        'side_area_icon_source' => 'icon_pack'
                    )
                )
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'        => $side_area_icon_pack_container,
                'type'          => 'select',
                'name'          => 'side_area_icon_pack',
                'default_value' => 'font_awesome',
                'label'         => esc_html__('Side Area Icon Pack', 'sarto'),
                'description'   => esc_html__('Choose icon pack for Side Area icon', 'sarto'),
                'options'       => sarto_edge_icon_collections()->getIconCollectionsExclude(array('linea_icons', 'dripicons', 'simple_line_icons'))
            )
        );

        $side_area_svg_icons_container = sarto_edge_add_admin_container(
            array(
                'parent'     => $side_area_panel,
                'name'       => 'side_area_svg_icons_container',
                'dependency' => array(
                    'show' => array(
                        'side_area_icon_source' => 'svg_path'
                    )
                )
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'      => $side_area_svg_icons_container,
                'type'        => 'textarea',
                'name'        => 'side_area_icon_svg_path',
                'label'       => esc_html__('Side Area Icon SVG Path', 'sarto'),
                'description' => esc_html__('Enter your Side Area icon SVG path here. Please remove version and id attributes from your SVG path because of HTML validation', 'sarto'),
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'      => $side_area_svg_icons_container,
                'type'        => 'textarea',
                'name'        => 'side_area_close_icon_svg_path',
                'label'       => esc_html__('Side Area Close Icon SVG Path', 'sarto'),
                'description' => esc_html__('Enter your Side Area close icon SVG path here. Please remove version and id attributes from your SVG path because of HTML validation', 'sarto'),
            )
        );

        $side_area_icon_style_group = sarto_edge_add_admin_group(
            array(
                'parent'      => $side_area_panel,
                'name'        => 'side_area_icon_style_group',
                'title'       => esc_html__('Side Area Icon Style', 'sarto'),
                'description' => esc_html__('Define styles for Side Area icon', 'sarto')
            )
        );

        $side_area_icon_style_row1 = sarto_edge_add_admin_row(
            array(
                'parent' => $side_area_icon_style_group,
                'name'   => 'side_area_icon_style_row1'
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent' => $side_area_icon_style_row1,
                'type'   => 'colorsimple',
                'name'   => 'side_area_icon_color',
                'label'  => esc_html__('Color', 'sarto')
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent' => $side_area_icon_style_row1,
                'type'   => 'colorsimple',
                'name'   => 'side_area_icon_hover_color',
                'label'  => esc_html__('Hover Color', 'sarto')
            )
        );

        $side_area_icon_style_row2 = sarto_edge_add_admin_row(
            array(
                'parent' => $side_area_icon_style_group,
                'name'   => 'side_area_icon_style_row2',
                'next'   => true
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent' => $side_area_icon_style_row2,
                'type'   => 'colorsimple',
                'name'   => 'side_area_close_icon_color',
                'label'  => esc_html__('Close Icon Color', 'sarto')
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent' => $side_area_icon_style_row2,
                'type'   => 'colorsimple',
                'name'   => 'side_area_close_icon_hover_color',
                'label'  => esc_html__('Close Icon Hover Color', 'sarto')
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'      => $side_area_panel,
                'type'        => 'color',
                'name'        => 'side_area_background_color',
                'label'       => esc_html__('Background Color', 'sarto'),
                'description' => esc_html__('Choose a background color for Side Area', 'sarto')
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'      => $side_area_panel,
                'type'        => 'text',
                'name'        => 'side_area_padding',
                'label'       => esc_html__('Padding', 'sarto'),
                'description' => esc_html__('Define padding for Side Area in format top right bottom left', 'sarto'),
                'args'        => array(
                    'col_width' => 3
                )
            )
        );

        sarto_edge_add_admin_field(
            array(
                'parent'        => $side_area_panel,
                'type'          => 'selectblank',
                'name'          => 'side_area_aligment',
                'default_value' => '',
                'label'         => esc_html__('Text Alignment', 'sarto'),
                'description'   => esc_html__('Choose text alignment for side area', 'sarto'),
                'options'       => array(
                    ''       => esc_html__('Default', 'sarto'),
                    'left'   => esc_html__('Left', 'sarto'),
                    'center' => esc_html__('Center', 'sarto'),
                    'right'  => esc_html__('Right', 'sarto')
                )
            )
        );
    }

    add_action('sarto_edge_options_map', 'sarto_edge_sidearea_options_map', 15);
}