<?php

if ( ! function_exists( 'sarto_edge_map_content_bottom_meta' ) ) {
	function sarto_edge_map_content_bottom_meta() {
		
		$content_bottom_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => apply_filters( 'sarto_edge_set_scope_for_meta_boxes', array( 'page', 'post' ), 'content_bottom_meta' ),
				'title' => esc_html__( 'Content Bottom', 'sarto' ),
				'name'  => 'content_bottom_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_enable_content_bottom_area_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Enable Content Bottom Area', 'sarto' ),
				'description'   => esc_html__( 'This option will enable Content Bottom area on pages', 'sarto' ),
				'parent'        => $content_bottom_meta_box,
				'options'       => sarto_edge_get_yes_no_select_array()
			)
		);
		
		$show_content_bottom_meta_container = sarto_edge_add_admin_container(
			array(
				'parent'          => $content_bottom_meta_box,
				'name'            => 'edgtf_show_content_bottom_meta_container',
				'dependency' => array(
					'show' => array(
						'edgtf_enable_content_bottom_area_meta' => 'yes'
					)
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_content_bottom_sidebar_custom_display_meta',
				'type'          => 'selectblank',
				'default_value' => '',
				'label'         => esc_html__( 'Sidebar to Display', 'sarto' ),
				'description'   => esc_html__( 'Choose a content bottom sidebar to display', 'sarto' ),
				'options'       => sarto_edge_get_custom_sidebars(),
				'parent'        => $show_content_bottom_meta_container,
				'args'          => array(
					'select2' => true
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'type'          => 'select',
				'name'          => 'edgtf_content_bottom_in_grid_meta',
				'default_value' => '',
				'label'         => esc_html__( 'Display in Grid', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will place content bottom in grid', 'sarto' ),
				'options'       => sarto_edge_get_yes_no_select_array(),
				'parent'        => $show_content_bottom_meta_container
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'type'        => 'color',
				'name'        => 'edgtf_content_bottom_background_color_meta',
				'label'       => esc_html__( 'Background Color', 'sarto' ),
				'description' => esc_html__( 'Choose a background color for content bottom area', 'sarto' ),
				'parent'      => $show_content_bottom_meta_container
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_content_bottom_meta', 71 );
}