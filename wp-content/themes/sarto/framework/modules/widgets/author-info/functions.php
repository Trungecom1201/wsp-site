<?php

if ( ! function_exists( 'sarto_edge_register_author_info_widget' ) ) {
	/**
	 * Function that register author info widget
	 */
	function sarto_edge_register_author_info_widget( $widgets ) {
		$widgets[] = 'SartoEdgeAuthorInfoWidget';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_author_info_widget' );
}