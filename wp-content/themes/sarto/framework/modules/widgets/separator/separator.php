<?php

class SartoEdgeSeparatorWidget extends SartoEdgeWidget {
	public function __construct() {
		parent::__construct(
			'edgtf_separator_widget',
			esc_html__( 'Edge Separator Widget', 'sarto' ),
			array( 'description' => esc_html__( 'Add a separator element to your widget areas', 'sarto' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'    => 'dropdown',
				'name'    => 'type',
				'title'   => esc_html__( 'Type', 'sarto' ),
				'options' => array(
					'normal'     => esc_html__( 'Normal', 'sarto' ),
					'full-width' => esc_html__( 'Full Width', 'sarto' )
				)
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'position',
				'title'   => esc_html__( 'Position', 'sarto' ),
				'options' => array(
					'center' => esc_html__( 'Center', 'sarto' ),
					'left'   => esc_html__( 'Left', 'sarto' ),
					'right'  => esc_html__( 'Right', 'sarto' )
				)
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'border_style',
				'title'   => esc_html__( 'Style', 'sarto' ),
				'options' => array(
					'solid'  => esc_html__( 'Solid', 'sarto' ),
					'dashed' => esc_html__( 'Dashed', 'sarto' ),
					'dotted' => esc_html__( 'Dotted', 'sarto' )
				)
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'color',
				'title' => esc_html__( 'Color', 'sarto' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'width',
				'title' => esc_html__( 'Width (px or %)', 'sarto' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'thickness',
				'title' => esc_html__( 'Thickness (px)', 'sarto' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'top_margin',
				'title' => esc_html__( 'Top Margin (px or %)', 'sarto' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'bottom_margin',
				'title' => esc_html__( 'Bottom Margin (px or %)', 'sarto' )
			)
		);
	}
	
	public function widget( $args, $instance ) {
		if ( ! is_array( $instance ) ) {
			$instance = array();
		}
		
		//prepare variables
		$params = '';
		
		//is instance empty?
		if ( is_array( $instance ) && count( $instance ) ) {
			//generate shortcode params
			foreach ( $instance as $key => $value ) {
				$params .= " $key='$value' ";
			}
		}
		
		echo '<div class="widget edgtf-separator-widget">';
			echo do_shortcode( "[edgtf_separator $params]" ); // XSS OK
		echo '</div>';
	}
}