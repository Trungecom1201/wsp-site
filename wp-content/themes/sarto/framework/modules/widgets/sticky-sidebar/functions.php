<?php

if(!function_exists('sarto_edge_register_sticky_sidebar_widget')) {
	/**
	 * Function that register sticky sidebar widget
	 */
	function sarto_edge_register_sticky_sidebar_widget($widgets) {
		$widgets[] = 'SartoEdgeStickySidebar';
		
		return $widgets;
	}
	
	add_filter('sarto_edge_register_widgets', 'sarto_edge_register_sticky_sidebar_widget');
}