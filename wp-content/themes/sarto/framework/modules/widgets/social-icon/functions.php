<?php

if ( ! function_exists( 'sarto_edge_register_social_icon_widget' ) ) {
	/**
	 * Function that register social icon widget
	 */
	function sarto_edge_register_social_icon_widget( $widgets ) {
		$widgets[] = 'SartoEdgeSocialIconWidget';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_social_icon_widget' );
}