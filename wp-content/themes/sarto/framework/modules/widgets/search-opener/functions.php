<?php

if ( ! function_exists( 'sarto_edge_register_search_opener_widget' ) ) {
	/**
	 * Function that register search opener widget
	 */
	function sarto_edge_register_search_opener_widget( $widgets ) {
		$widgets[] = 'SartoEdgeSearchOpener';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_search_opener_widget' );
}