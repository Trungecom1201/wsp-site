<?php

if ( ! function_exists( 'sarto_edge_sidebar_options_map' ) ) {
	function sarto_edge_sidebar_options_map() {
		
		sarto_edge_add_admin_page(
			array(
				'slug'  => '_sidebar_page',
				'title' => esc_html__( 'Sidebar Area', 'sarto' ),
				'icon'  => 'fa fa-indent'
			)
		);
		
		$sidebar_panel = sarto_edge_add_admin_panel(
			array(
				'title' => esc_html__( 'Sidebar Area', 'sarto' ),
				'name'  => 'sidebar',
				'page'  => '_sidebar_page'
			)
		);
		
		sarto_edge_add_admin_field( array(
			'name'          => 'sidebar_layout',
			'type'          => 'select',
			'label'         => esc_html__( 'Sidebar Layout', 'sarto' ),
			'description'   => esc_html__( 'Choose a sidebar layout for pages', 'sarto' ),
			'parent'        => $sidebar_panel,
			'default_value' => 'no-sidebar',
            'options'       => sarto_edge_get_custom_sidebars_options()
		) );
		
		$sarto_custom_sidebars = sarto_edge_get_custom_sidebars();
		if ( count( $sarto_custom_sidebars ) > 0 ) {
			sarto_edge_add_admin_field( array(
				'name'        => 'custom_sidebar_area',
				'type'        => 'selectblank',
				'label'       => esc_html__( 'Sidebar to Display', 'sarto' ),
				'description' => esc_html__( 'Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'sarto' ),
				'parent'      => $sidebar_panel,
				'options'     => $sarto_custom_sidebars,
				'args'        => array(
					'select2' => true
				)
			) );
		}
	}
	
	add_action( 'sarto_edge_options_map', 'sarto_edge_sidebar_options_map', 9 );
}