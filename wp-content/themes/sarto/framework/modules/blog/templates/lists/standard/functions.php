<?php

if ( ! function_exists( 'sarto_edge_register_blog_standard_template_file' ) ) {
	/**
	 * Function that register blog standard template
	 */
	function sarto_edge_register_blog_standard_template_file( $templates ) {
		$templates['blog-standard'] = esc_html__( 'Blog: Standard', 'sarto' );
		
		return $templates;
	}
	
	add_filter( 'sarto_edge_register_blog_templates', 'sarto_edge_register_blog_standard_template_file' );
}

if ( ! function_exists( 'sarto_edge_set_blog_standard_type_global_option' ) ) {
	/**
	 * Function that set blog list type value for global blog option map
	 */
	function sarto_edge_set_blog_standard_type_global_option( $options ) {
		$options['standard'] = esc_html__( 'Blog: Standard', 'sarto' );
		
		return $options;
	}
	
	add_filter( 'sarto_edge_blog_list_type_global_option', 'sarto_edge_set_blog_standard_type_global_option' );
}