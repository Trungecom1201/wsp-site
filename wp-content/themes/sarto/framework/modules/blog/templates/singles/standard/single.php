<?php

sarto_edge_get_single_post_format_html($blog_single_type);

do_action('sarto_edge_after_article_content');

sarto_edge_get_module_template_part('templates/parts/single/author-info', 'blog');

sarto_edge_get_module_template_part('templates/parts/single/single-navigation', 'blog');

sarto_edge_get_module_template_part('templates/parts/single/related-posts', 'blog', '', $single_info_params);

sarto_edge_get_module_template_part('templates/parts/single/comments', 'blog');