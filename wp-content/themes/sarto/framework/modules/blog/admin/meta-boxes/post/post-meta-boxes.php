<?php

/*** Post Settings ***/

if ( ! function_exists( 'sarto_edge_map_post_meta' ) ) {
	function sarto_edge_map_post_meta() {
		
		$post_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Post', 'sarto' ),
				'name'  => 'post-meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_blog_single_sidebar_layout_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Sidebar Layout', 'sarto' ),
				'description'   => esc_html__( 'Choose a sidebar layout for Blog single page', 'sarto' ),
				'default_value' => '',
				'parent'        => $post_meta_box,
                'options'       => sarto_edge_get_custom_sidebars_options( true )
			)
		);
		
		$sarto_custom_sidebars = sarto_edge_get_custom_sidebars();
		if ( count( $sarto_custom_sidebars ) > 0 ) {
			sarto_edge_create_meta_box_field( array(
				'name'        => 'edgtf_blog_single_custom_sidebar_area_meta',
				'type'        => 'selectblank',
				'label'       => esc_html__( 'Sidebar to Display', 'sarto' ),
				'description' => esc_html__( 'Choose a sidebar to display on Blog single page. Default sidebar is "Sidebar"', 'sarto' ),
				'parent'      => $post_meta_box,
				'options'     => sarto_edge_get_custom_sidebars(),
				'args' => array(
					'select2' => true
				)
			) );
		}
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_blog_list_featured_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Blog List Image', 'sarto' ),
				'description' => esc_html__( 'Choose an Image for displaying in blog list. If not uploaded, featured image will be shown.', 'sarto' ),
				'parent'      => $post_meta_box
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_blog_masonry_gallery_fixed_dimensions_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Dimensions for Fixed Proportion', 'sarto' ),
				'description'   => esc_html__( 'Choose image layout when it appears in Masonry lists in fixed proportion', 'sarto' ),
				'default_value' => 'small',
				'parent'        => $post_meta_box,
				'options'       => array(
					'small'              => esc_html__( 'Small', 'sarto' ),
					'large-width'        => esc_html__( 'Large Width', 'sarto' ),
					'large-height'       => esc_html__( 'Large Height', 'sarto' ),
					'large-width-height' => esc_html__( 'Large Width/Height', 'sarto' )
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_blog_masonry_gallery_original_dimensions_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Dimensions for Original Proportion', 'sarto' ),
				'description'   => esc_html__( 'Choose image layout when it appears in Masonry lists in original proportion', 'sarto' ),
				'default_value' => 'default',
				'parent'        => $post_meta_box,
				'options'       => array(
					'default'     => esc_html__( 'Default', 'sarto' ),
					'large-width' => esc_html__( 'Large Width', 'sarto' )
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_show_title_area_blog_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will show title area on your single post page', 'sarto' ),
				'parent'        => $post_meta_box,
				'options'       => sarto_edge_get_yes_no_select_array()
			)
		);

		do_action('sarto_edge_blog_post_meta', $post_meta_box);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_post_meta', 20 );
}
