<?php

if ( ! function_exists( 'sarto_edge_map_post_quote_meta' ) ) {
	function sarto_edge_map_post_quote_meta() {
		$quote_post_format_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Quote Post Format', 'sarto' ),
				'name'  => 'post_format_quote_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_post_quote_text_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Text', 'sarto' ),
				'description' => esc_html__( 'Enter Quote text', 'sarto' ),
				'parent'      => $quote_post_format_meta_box
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_post_quote_author_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Author', 'sarto' ),
				'description' => esc_html__( 'Enter Quote author', 'sarto' ),
				'parent'      => $quote_post_format_meta_box
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_post_quote_meta', 25 );
}