<?php

if ( ! function_exists( 'sarto_edge_woocommerce_options_map' ) ) {
	
	/**
	 * Add Woocommerce options page
	 */
	function sarto_edge_woocommerce_options_map() {
		
		sarto_edge_add_admin_page(
			array(
				'slug'  => '_woocommerce_page',
				'title' => esc_html__( 'Woocommerce', 'sarto' ),
				'icon'  => 'fa fa-shopping-cart'
			)
		);
		
		/**
		 * Product List Settings
		 */
		$panel_product_list = sarto_edge_add_admin_panel(
			array(
				'page'  => '_woocommerce_page',
				'name'  => 'panel_product_list',
				'title' => esc_html__( 'Product List', 'sarto' )
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'edgtf_woo_product_list_columns',
				'label'         => esc_html__( 'Product List Columns', 'sarto' ),
				'default_value' => 'edgtf-woocommerce-columns-3',
				'description'   => esc_html__( 'Choose number of columns for main shop page', 'sarto' ),
				'options'       => array(
					'edgtf-woocommerce-columns-3' => esc_html__( '3 Columns', 'sarto' ),
					'edgtf-woocommerce-columns-4' => esc_html__( '4 Columns', 'sarto' )
				),
				'parent'        => $panel_product_list,
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'edgtf_woo_product_list_columns_space',
				'label'         => esc_html__( 'Space Between Items', 'sarto' ),
				'description'   => esc_html__( 'Select space between items for product listing and related products on single product', 'sarto' ),
				'default_value' => 'normal',
				'options'       => sarto_edge_get_space_between_items_array(),
				'parent'        => $panel_product_list,
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'edgtf_woo_product_list_info_position',
				'label'         => esc_html__( 'Product Info Position', 'sarto' ),
				'default_value' => 'info_below_image',
				'description'   => esc_html__( 'Select product info position for product listing and related products on single product', 'sarto' ),
				'options'       => array(
					'info_below_image'    => esc_html__( 'Info Below Image', 'sarto' ),
					'info_on_image_hover' => esc_html__( 'Info On Image Hover', 'sarto' )
				),
				'parent'        => $panel_product_list,
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'text',
				'name'          => 'edgtf_woo_products_per_page',
				'label'         => esc_html__( 'Number of products per page', 'sarto' ),
				'description'   => esc_html__( 'Set number of products on shop page', 'sarto' ),
				'parent'        => $panel_product_list,
				'args'          => array(
					'col_width' => 3
				)
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'edgtf_products_list_title_tag',
				'label'         => esc_html__( 'Products Title Tag', 'sarto' ),
				'default_value' => 'h5',
				'options'       => sarto_edge_get_title_tag(),
				'parent'        => $panel_product_list,
			)
		);
		
		/**
		 * Single Product Settings
		 */
		$panel_single_product = sarto_edge_add_admin_panel(
			array(
				'page'  => '_woocommerce_page',
				'name'  => 'panel_single_product',
				'title' => esc_html__( 'Single Product', 'sarto' )
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'show_title_area_woo',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will show title area on single post pages', 'sarto' ),
				'parent'        => $panel_single_product,
				'options'       => sarto_edge_get_yes_no_select_array(),
				'args'          => array(
					'col_width' => 3
				)
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'edgtf_single_product_title_tag',
				'default_value' => 'h4',
				'label'         => esc_html__( 'Single Product Title Tag', 'sarto' ),
				'options'       => sarto_edge_get_title_tag(),
				'parent'        => $panel_single_product,
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'woo_number_of_thumb_images',
				'default_value' => '4',
				'label'         => esc_html__( 'Number of Thumbnail Images per Row', 'sarto' ),
				'options'       => array(
					'4' => esc_html__( 'Four', 'sarto' ),
					'3' => esc_html__( 'Three', 'sarto' ),
					'2' => esc_html__( 'Two', 'sarto' )
				),
				'parent'        => $panel_single_product
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'woo_set_thumb_images_position',
				'default_value' => 'below-image',
				'label'         => esc_html__( 'Set Thumbnail Images Position', 'sarto' ),
				'options'       => array(
					'below-image'  => esc_html__( 'Below Featured Image', 'sarto' ),
					'on-left-side' => esc_html__( 'On The Left Side Of Featured Image', 'sarto' )
				),
				'parent'        => $panel_single_product
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'woo_enable_single_product_zoom_image',
				'default_value' => 'no',
				'label'         => esc_html__( 'Enable Zoom Maginfier', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will show magnifier image on featured image hover', 'sarto' ),
				'parent'        => $panel_single_product,
				'options'       => sarto_edge_get_yes_no_select_array( false ),
				'args'          => array(
					'col_width' => 3
				)
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'woo_set_single_images_behavior',
				'default_value' => 'pretty-photo',
				'label'         => esc_html__( 'Set Images Behavior', 'sarto' ),
				'options'       => array(
					'pretty-photo' => esc_html__( 'Pretty Photo Lightbox', 'sarto' ),
					'photo-swipe'  => esc_html__( 'Photo Swipe Lightbox', 'sarto' )
				),
				'parent'        => $panel_single_product
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'edgtf_woo_related_products_columns',
				'label'         => esc_html__( 'Related Products Columns', 'sarto' ),
				'default_value' => 'edgtf-woocommerce-columns-3',
				'description'   => esc_html__( 'Choose number of columns for related products on single product page', 'sarto' ),
				'options'       => array(
					'edgtf-woocommerce-columns-3' => esc_html__( '3 Columns', 'sarto' ),
					'edgtf-woocommerce-columns-4' => esc_html__( '4 Columns', 'sarto' )
				),
				'parent'        => $panel_single_product,
			)
		);

		do_action('sarto_edge_woocommerce_additional_options_map');
	}
	
	add_action( 'sarto_edge_options_map', 'sarto_edge_woocommerce_options_map', 21 );
}