<?php

if ( ! function_exists( 'sarto_edge_add_product_info_shortcode' ) ) {
	function sarto_edge_add_product_info_shortcode( $shortcodes_class_name ) {
		$shortcodes = array(
			'SartoCore\CPT\Shortcodes\ProductInfo\ProductInfo',
		);
		
		$shortcodes_class_name = array_merge( $shortcodes_class_name, $shortcodes );
		
		return $shortcodes_class_name;
	}
	
	if ( sarto_edge_core_plugin_installed() ) {
		add_filter( 'sarto_core_filter_add_vc_shortcode', 'sarto_edge_add_product_info_shortcode' );
	}
}

if ( ! function_exists( 'sarto_edge_add_product_info_into_shortcodes_list' ) ) {
	function sarto_edge_add_product_info_into_shortcodes_list( $woocommerce_shortcodes ) {
		$woocommerce_shortcodes[] = 'edgtf_product_info';
		
		return $woocommerce_shortcodes;
	}
	
	add_filter( 'sarto_edge_woocommerce_shortcodes_list', 'sarto_edge_add_product_info_into_shortcodes_list' );
}