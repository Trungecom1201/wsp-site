<?php

if ( ! function_exists( 'sarto_edge_dropdown_cart_icon_styles' ) ) {
	/**
	 * Generates styles for dropdown cart icon
	 */
	function sarto_edge_dropdown_cart_icon_styles() {
		$icon_color       = sarto_edge_options()->getOptionValue( 'dropdown_cart_icon_color' );
		$icon_hover_color = sarto_edge_options()->getOptionValue( 'dropdown_cart_hover_color' );
		
		if ( ! empty( $icon_color ) ) {
			echo sarto_edge_dynamic_css( '.edgtf-shopping-cart-holder .edgtf-header-cart a', array( 'color' => $icon_color ) );
		}
		
		if ( ! empty( $icon_hover_color ) ) {
			echo sarto_edge_dynamic_css( '.edgtf-shopping-cart-holder .edgtf-header-cart a:hover', array( 'color' => $icon_hover_color ) );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_dropdown_cart_icon_styles' );
}