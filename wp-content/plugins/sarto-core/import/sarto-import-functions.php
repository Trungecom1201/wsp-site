<?php

if ( ! function_exists( 'sarto_core_import_object' ) ) {
	function sarto_core_import_object() {
		$sarto_core_import_object = new SartoCoreImport();
	}
	
	add_action( 'init', 'sarto_core_import_object' );
}

if ( ! function_exists( 'sarto_core_data_import' ) ) {
	function sarto_core_data_import() {
		$importObject = SartoCoreImport::getInstance();
		
		if ( $_POST['import_attachments'] == 1 ) {
			$importObject->attachments = true;
		} else {
			$importObject->attachments = false;
		}
		
		$folder = "sarto/";
		if ( ! empty( $_POST['example'] ) ) {
			$folder = $_POST['example'] . "/";
		}
		
		$importObject->import_content( $folder . $_POST['xml'] );
		
		die();
	}
	
	add_action( 'wp_ajax_sarto_core_data_import', 'sarto_core_data_import' );
}

if ( ! function_exists( 'sarto_core_widgets_import' ) ) {
	function sarto_core_widgets_import() {
		$importObject = SartoCoreImport::getInstance();
		
		$folder = "sarto/";
		if ( ! empty( $_POST['example'] ) ) {
			$folder = $_POST['example'] . "/";
		}
		
		$importObject->import_widgets( $folder . 'widgets.txt', $folder . 'custom_sidebars.txt' );
		
		die();
	}
	
	add_action( 'wp_ajax_sarto_core_widgets_import', 'sarto_core_widgets_import' );
}

if ( ! function_exists( 'sarto_core_options_import' ) ) {
	function sarto_core_options_import() {
		$importObject = SartoCoreImport::getInstance();
		
		$folder = "sarto/";
		if ( ! empty( $_POST['example'] ) ) {
			$folder = $_POST['example'] . "/";
		}
		
		$importObject->import_options( $folder . 'options.txt' );
		
		die();
	}
	
	add_action( 'wp_ajax_sarto_core_options_import', 'sarto_core_options_import' );
}

if ( ! function_exists( 'sarto_core_other_import' ) ) {
	function sarto_core_other_import() {
		$importObject = SartoCoreImport::getInstance();
		
		$folder = "sarto/";
		if ( ! empty( $_POST['example'] ) ) {
			$folder = $_POST['example'] . "/";
		}
		
		$importObject->import_options( $folder . 'options.txt' );
		$importObject->import_widgets( $folder . 'widgets.txt', $folder . 'custom_sidebars.txt' );
		$importObject->import_menus( $folder . 'menus.txt' );
		$importObject->import_settings_pages( $folder . 'settingpages.txt' );

		$importObject->edgtf_update_meta_fields_after_import($folder);
		$importObject->edgtf_update_options_after_import($folder);

		if ( sarto_core_is_revolution_slider_installed() ) {
			$importObject->rev_slider_import( $folder );
		}
		
		die();
	}
	
	add_action( 'wp_ajax_sarto_core_other_import', 'sarto_core_other_import' );
}