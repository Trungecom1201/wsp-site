<?php

if ( ! function_exists( 'sarto_core_map_testimonials_meta' ) ) {
	function sarto_core_map_testimonials_meta() {
		$testimonial_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => array( 'testimonials' ),
				'title' => esc_html__( 'Testimonial', 'sarto-core' ),
				'name'  => 'testimonial_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_testimonial_title',
				'type'        => 'text',
				'label'       => esc_html__( 'Title', 'sarto-core' ),
				'description' => esc_html__( 'Enter testimonial title', 'sarto-core' ),
				'parent'      => $testimonial_meta_box,
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_testimonial_text',
				'type'        => 'text',
				'label'       => esc_html__( 'Text', 'sarto-core' ),
				'description' => esc_html__( 'Enter testimonial text', 'sarto-core' ),
				'parent'      => $testimonial_meta_box,
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_testimonial_author',
				'type'        => 'text',
				'label'       => esc_html__( 'Author', 'sarto-core' ),
				'description' => esc_html__( 'Enter author name', 'sarto-core' ),
				'parent'      => $testimonial_meta_box,
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_testimonial_author_position',
				'type'        => 'text',
				'label'       => esc_html__( 'Author Position', 'sarto-core' ),
				'description' => esc_html__( 'Enter author job position', 'sarto-core' ),
				'parent'      => $testimonial_meta_box,
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_core_map_testimonials_meta', 95 );
}