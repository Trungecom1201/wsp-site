<?php

namespace SartoCore\CPT\Portfolio\Shortcodes;

use SartoCore\Lib;

/**
 * Class PortfolioTabsSlider
 * @package EdgeCore\CPT\Portfolio\Shortcodes
 */
class PortfolioTabsSlider implements Lib\ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'edgtf_portfolio_tabs_slider';

		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 *
	 * @see vc_map()
	 */
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map( array(
				'name'                      => esc_html__( 'Edge Portfolio Tabs Slider', 'sarto-core' ),
				'base'                      => $this->base,
				'category'                  => 'by SARTO',
				'icon'                      => 'icon-wpb-portfolio-tabs-slider extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params'                    => array(
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Portfolio Categories Tab 1', 'sarto-core' ),
						'param_name'  => 'categories_tab_1',
						'value'       => sarto_core_get_portfolio_categories(),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Portfolio Categories Tab 2', 'sarto-core' ),
						'param_name'  => 'categories_tab_2',
						'value'       => sarto_core_get_portfolio_categories(),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Portfolio Categories Tab 3', 'sarto-core' ),
						'param_name'  => 'categories_tab_3',
						'value'       => sarto_core_get_portfolio_categories(),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Portfolio Categories Tab 4', 'sarto-core' ),
						'param_name'  => 'categories_tab_4',
						'value'       => sarto_core_get_portfolio_categories(),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Portfolio Categories Tab 5', 'sarto-core' ),
						'param_name'  => 'categories_tab_5',
						'value'       => sarto_core_get_portfolio_categories(),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'title_underscore',
						'heading'     => esc_html__( 'Enable SVG Underscore below Title', 'sarto-core' ),
						'value'       => array_flip( sarto_edge_get_yes_no_select_array( false, true ) ),
						'save_always' => true,
						'group'       => esc_html__( 'Title Style', 'sarto-core' )
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Number of Columns', 'sarto-core' ),
						'param_name'  => 'number_of_columns',
						'value'       => array(
							esc_html__( 'Default', 'sarto-core' ) => '',
							esc_html__( 'One', 'sarto-core' )     => '1',
							esc_html__( 'Two', 'sarto-core' )     => '2',
							esc_html__( 'Three', 'sarto-core' )   => '3',
							esc_html__( 'Four', 'sarto-core' )    => '4',
							esc_html__( 'Five', 'sarto-core' )    => '5'
						),
						'description' => esc_html__( 'Set columns number for portfolio lists inside tabs. Default value is Three', 'sarto-core' ),
						'admin_label' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Space Between Portfolio Items', 'sarto-core' ),
						'param_name'  => 'space_between_items',
						'value'       => array_flip( sarto_edge_get_space_between_items_array() ),
						'admin_label' => true,
						'save_always' => true
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Number of Portfolios Items', 'sarto-core' ),
						'param_name'  => 'number_of_items',
						'admin_label' => true,
						'description' => esc_html__( 'Set number of items for your portfolio list inside tabs. Enter -1 to show all.', 'sarto-core' ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Image Proportions', 'sarto-core' ),
						'param_name'  => 'image_proportions',
						'value'       => array(
							esc_html__( 'Original', 'sarto-core' )  => 'full',
							esc_html__( 'Square', 'sarto-core' )    => 'square',
							esc_html__( 'Landscape', 'sarto-core' ) => 'landscape',
							esc_html__( 'Portrait', 'sarto-core' )  => 'portrait',
							esc_html__( 'Medium', 'sarto-core' )    => 'medium',
							esc_html__( 'Large', 'sarto-core' )     => 'large'
						),
						'description' => esc_html__( 'Set image proportions for your portfolio list.', 'sarto-core' ),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Order By', 'sarto-core' ),
						'param_name'  => 'order_by',
						'value'       => array_flip( sarto_edge_get_query_order_by_array() ),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Order', 'sarto-core' ),
						'param_name'  => 'order',
						'value'       => array_flip( sarto_edge_get_query_order_array() ),
						'save_always' => true
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'item_style',
						'heading'    => esc_html__( 'Item Style', 'sarto-core' ),
						'value'      => array(
							esc_html__( 'Standard - Shader', 'sarto-core' )                 => 'standard-shader',
							esc_html__( 'Standard - Switch Featured Images', 'sarto-core' ) => 'standard-switch-images',
							esc_html__( 'Gallery - Overlay', 'sarto-core' )                 => 'gallery-overlay',
							esc_html__( 'Gallery - Slide From Image Bottom', 'sarto-core' ) => 'gallery-slide-from-image-bottom'
						),
						'group'      => esc_html__( 'Content Layout', 'sarto-core' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'enable_title',
						'heading'    => esc_html__( 'Enable Title', 'sarto-core' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false, true ) ),
						'group'      => esc_html__( 'Content Layout', 'sarto-core' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'title_tag',
						'heading'    => esc_html__( 'Title Tag', 'sarto-core' ),
						'value'      => array_flip( sarto_edge_get_title_tag( true ) ),
						'dependency' => array( 'element' => 'enable_title', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Content Layout', 'sarto-core' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'title_text_transform',
						'heading'    => esc_html__( 'Title Text Transform', 'sarto-core' ),
						'value'      => array_flip( sarto_edge_get_text_transform_array( true ) ),
						'dependency' => array( 'element' => 'enable_title', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Content Layout', 'sarto-core' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'enable_category',
						'heading'    => esc_html__( 'Enable Category', 'sarto-core' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false, true ) ),
						'group'      => esc_html__( 'Content Layout', 'sarto-core' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'enable_count_images',
						'heading'    => esc_html__( 'Enable Number of Images', 'sarto-core' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false, true ) ),
						'dependency' => array( 'element' => 'type', 'value' => array( 'gallery' ) ),
						'group'      => esc_html__( 'Content Layout', 'sarto-core' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'enable_excerpt',
						'heading'    => esc_html__( 'Enable Excerpt', 'sarto-core' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false ) ),
						'group'      => esc_html__( 'Content Layout', 'sarto-core' )
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'excerpt_length',
						'heading'     => esc_html__( 'Excerpt Length', 'sarto-core' ),
						'description' => esc_html__( 'Number of characters', 'sarto-core' ),
						'dependency'  => array( 'element' => 'enable_excerpt', 'value' => array( 'yes' ) ),
						'group'       => esc_html__( 'Content Layout', 'sarto-core' )
					)
				)
			) );
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render( $atts, $content = null ) {
		$args = array(
			'categories_tab_1'          => '',
			'categories_tab_2'          => '',
			'categories_tab_3'          => '',
			'categories_tab_4'          => '',
			'categories_tab_5'          => '',
			'title_underscore'          => '',
			'number_of_columns' => '3',
			'space_between_items'       => 'normal',
			'number_of_items'           => '9',
			'image_proportions'         => 'full',
			'order_by'                  => 'date',
			'order'                     => 'ASC',
			'item_style'                => 'standard-shader',
			'enable_title'              => 'yes',
			'title_tag'                 => 'h5',
			'title_text_transform'     => '',
			'enable_category'          => 'yes',
			'enable_count_images'      => 'yes',
			'enable_excerpt'           => 'no',
			'excerpt_length'           => '20'
		);

		$params = shortcode_atts( $args, $atts );

		extract( $params );

		$params['type']             = 'gallery';
		$params['holder_classes']   = $this->getHolderClasses( $params, $args );
		$params['categories_array'] = $this->getAllCategories( $params );
		$params['categories_classes']   = $this->getCategoryClasses( $params );

		$html = '';

		// load slider
		$html .= sarto_core_get_cpt_shortcode_module_template_part( 'portfolio', 'portfolio-tabs-slider', 'portfolio-tabs-slider-holder', '', $params);

		return $html;
	}

	/**
	 * Holder Classes
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getHolderClasses( $params ) {
		$holderClasses = array();

		$holderClasses[] = ! empty( $params['custom_class'] ) ? esc_attr( $params['custom_class'] ) : '';
		$holderClasses[] = $params['title_underscore'] === 'yes' ? 'edgtf-st-title-underscore' : '';

		return implode( ' ', $holderClasses );
	}

	/**
	 * Holder Classes
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getCategoryClasses( $params ) {
		$holderClasses = array();

		$holderClasses[] = 'edgtf-columns-' . count($params['categories_array']);

		return implode( ' ', $holderClasses );
	}

	/**
	 * Holder Classes
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getAllCategories( $params ) {

		$categories = array();

		for($i = 1; $i <= 5; $i++) {
			if($params['categories_tab_'. $i] != '') {
				array_push($categories, $params['categories_tab_'. $i]);
			};
		}

		return $categories;
	}
}