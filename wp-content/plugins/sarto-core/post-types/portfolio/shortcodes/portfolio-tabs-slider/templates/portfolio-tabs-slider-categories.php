<div class="edgtf-pts-menu <?php echo esc_attr( $categories_classes ) ?>">
    <ul>
		<?php
		foreach ( $categories_array as $category ) {
		    if( $category !== '' && (get_term_by( 'slug', $category, 'portfolio-category' ) !== false)) { ?>
            <li>
                <a href="#">
					<span class="edgtf-pts-menu-text">
                    <?php
					echo get_term_by( 'slug', $category, 'portfolio-category' )->name; ?>
                    </span>
                    <span class="edgtf-pts-menu-separator">
					    <?php echo sarto_edge_get_title_underscore_svg_image( 109, 12 ); ?>
					</span>
                </a>
            </li>
		<?php }
		}
		?>
    </ul>
</div>