<?php

if ( ! function_exists( 'sarto_core_map_portfolio_settings_meta' ) ) {
	function sarto_core_map_portfolio_settings_meta() {
		$meta_box = sarto_edge_create_meta_box( array(
			'scope' => 'portfolio-item',
			'title' => esc_html__( 'Portfolio Settings', 'sarto-core' ),
			'name'  => 'portfolio_settings_meta_box'
		) );
		
		sarto_edge_create_meta_box_field( array(
			'name'        => 'edgtf_portfolio_single_template_meta',
			'type'        => 'select',
			'label'       => esc_html__( 'Portfolio Type', 'sarto-core' ),
			'description' => esc_html__( 'Choose a default type for Single Project pages', 'sarto-core' ),
			'parent'      => $meta_box,
			'options'     => array(
				''                  => esc_html__( 'Default', 'sarto-core' ),
				'huge-images'       => esc_html__( 'Portfolio Full Width Images', 'sarto-core' ),
				'images'            => esc_html__( 'Portfolio Images', 'sarto-core' ),
				'small-images'      => esc_html__( 'Portfolio Small Images', 'sarto-core' ),
				'slider'            => esc_html__( 'Portfolio Slider', 'sarto-core' ),
				'small-slider'      => esc_html__( 'Portfolio Small Slider', 'sarto-core' ),
				'gallery'           => esc_html__( 'Portfolio Gallery', 'sarto-core' ),
				'small-gallery'     => esc_html__( 'Portfolio Small Gallery', 'sarto-core' ),
				'masonry'           => esc_html__( 'Portfolio Masonry', 'sarto-core' ),
				'small-masonry'     => esc_html__( 'Portfolio Small Masonry', 'sarto-core' ),
				'custom'            => esc_html__( 'Portfolio Custom', 'sarto-core' ),
				'full-width-custom' => esc_html__( 'Portfolio Full Width Custom', 'sarto-core' )
			)
		) );
		
		/***************** Gallery Layout *****************/
		
		$gallery_type_meta_container = sarto_edge_add_admin_container(
			array(
				'parent'          => $meta_box,
				'name'            => 'edgtf_gallery_type_meta_container',
				'dependency' => array(
					'show' => array(
						'edgtf_portfolio_single_template_meta'  => array(
							'gallery',
							'small-gallery'
						)
					)
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_portfolio_single_gallery_columns_number_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Number of Columns', 'sarto-core' ),
				'default_value' => '',
				'description'   => esc_html__( 'Set number of columns for portfolio gallery type', 'sarto-core' ),
				'parent'        => $gallery_type_meta_container,
				'options'       => array(
					''      => esc_html__( 'Default', 'sarto-core' ),
					'two'   => esc_html__( '2 Columns', 'sarto-core' ),
					'three' => esc_html__( '3 Columns', 'sarto-core' ),
					'four'  => esc_html__( '4 Columns', 'sarto-core' )
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_portfolio_single_gallery_space_between_items_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Space Between Items', 'sarto-core' ),
				'description'   => esc_html__( 'Set space size between columns for portfolio gallery type', 'sarto-core' ),
				'default_value' => '',
				'options'       => sarto_edge_get_space_between_items_array( true ),
				'parent'        => $gallery_type_meta_container
			)
		);
		
		/***************** Gallery Layout *****************/
		
		/***************** Masonry Layout *****************/
		
		$masonry_type_meta_container = sarto_edge_add_admin_container(
			array(
				'parent'          => $meta_box,
				'name'            => 'edgtf_masonry_type_meta_container',
				'dependency' => array(
					'show' => array(
						'edgtf_portfolio_single_template_meta'  => array(
							'masonry',
							'small-masonry'
						)
					)
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_portfolio_single_masonry_columns_number_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Number of Columns', 'sarto-core' ),
				'default_value' => '',
				'description'   => esc_html__( 'Set number of columns for portfolio masonry type', 'sarto-core' ),
				'parent'        => $masonry_type_meta_container,
				'options'       => array(
					''      => esc_html__( 'Default', 'sarto-core' ),
					'two'   => esc_html__( '2 Columns', 'sarto-core' ),
					'three' => esc_html__( '3 Columns', 'sarto-core' ),
					'four'  => esc_html__( '4 Columns', 'sarto-core' )
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_portfolio_single_masonry_space_between_items_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Space Between Items', 'sarto-core' ),
				'description'   => esc_html__( 'Set space size between columns for portfolio masonry type', 'sarto-core' ),
				'default_value' => '',
				'options'       => sarto_edge_get_space_between_items_array( true ),
				'parent'        => $masonry_type_meta_container
			)
		);
		
		/***************** Masonry Layout *****************/
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_show_title_area_portfolio_single_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'sarto-core' ),
				'description'   => esc_html__( 'Enabling this option will show title area on your single portfolio page', 'sarto-core' ),
				'parent'        => $meta_box,
				'options'       => sarto_edge_get_yes_no_select_array()
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'portfolio_info_top_padding',
				'type'        => 'text',
				'label'       => esc_html__( 'Portfolio Info Top Padding', 'sarto-core' ),
				'description' => esc_html__( 'Set top padding for portfolio info elements holder. This option works only for Portfolio Images, Slider, Gallery and Masonry portfolio types', 'sarto-core' ),
				'parent'      => $meta_box,
				'args'        => array(
					'col_width' => 3,
					'suffix'    => 'px'
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'portfolio_external_link',
				'type'        => 'text',
				'label'       => esc_html__( 'Portfolio External Link', 'sarto-core' ),
				'description' => esc_html__( 'Enter URL to link from Portfolio List page', 'sarto-core' ),
				'parent'      => $meta_box,
				'args'        => array(
					'col_width' => 3
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_portfolio_featured_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Featured Image', 'sarto-core' ),
				'description' => esc_html__( 'Choose an image for Portfolio Lists shortcode where Hover Type option is Switch Featured Images', 'sarto-core' ),
				'parent'      => $meta_box
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_portfolio_masonry_fixed_dimensions_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Dimensions for Masonry - Image Fixed Proportion', 'sarto-core' ),
				'description'   => esc_html__( 'Choose image layout when it appears in Masonry type portfolio lists where image proportion is fixed', 'sarto-core' ),
				'default_value' => '',
				'parent'        => $meta_box,
				'options'       => array(
					''                   => esc_html__( 'Default', 'sarto-core' ),
					'small'              => esc_html__( 'Small', 'sarto-core' ),
					'large-width'        => esc_html__( 'Large Width', 'sarto-core' ),
					'large-height'       => esc_html__( 'Large Height', 'sarto-core' ),
					'large-width-height' => esc_html__( 'Large Width/Height', 'sarto-core' )
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_portfolio_masonry_original_dimensions_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Dimensions for Masonry - Image Original Proportion', 'sarto-core' ),
				'description'   => esc_html__( 'Choose image layout when it appears in Masonry type portfolio lists where image proportion is original', 'sarto-core' ),
				'default_value' => 'default',
				'parent'        => $meta_box,
				'options'       => array(
					'default'     => esc_html__( 'Default', 'sarto-core' ),
					'large-width' => esc_html__( 'Large Width', 'sarto-core' )
				)
			)
		);
		
		$all_pages = array();
		$pages     = get_pages();
		foreach ( $pages as $page ) {
			$all_pages[ $page->ID ] = $page->post_title;
		}
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'portfolio_single_back_to_link',
				'type'        => 'select',
				'label'       => esc_html__( '"Back To" Link', 'sarto-core' ),
				'description' => esc_html__( 'Choose "Back To" page to link from portfolio Single Project page', 'sarto-core' ),
				'parent'      => $meta_box,
				'options'     => $all_pages,
				'args'        => array(
					'select2' => true
				)
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_core_map_portfolio_settings_meta', 41 );
}