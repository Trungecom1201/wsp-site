<?php
get_header();
sarto_edge_get_title();
do_action( 'sarto_edge_before_main_content' ); ?>
<div class="edgtf-container edgtf-default-page-template">
	<?php do_action( 'sarto_edge_after_container_open' ); ?>
	<div class="edgtf-container-inner clearfix">
		<?php
			$sarto_taxonomy_id   = get_queried_object_id();
			$sarto_taxonomy_type = is_tax( 'portfolio-tag' ) ? 'portfolio-tag' : 'portfolio-category';
			$sarto_taxonomy      = ! empty( $sarto_taxonomy_id ) ? get_term_by( 'id', $sarto_taxonomy_id, $sarto_taxonomy_type ) : '';
			$sarto_taxonomy_slug = ! empty( $sarto_taxonomy ) ? $sarto_taxonomy->slug : '';
			$sarto_taxonomy_name = ! empty( $sarto_taxonomy ) ? $sarto_taxonomy->taxonomy : '';
			
			sarto_core_get_archive_portfolio_list( $sarto_taxonomy_slug, $sarto_taxonomy_name );
		?>
	</div>
	<?php do_action( 'sarto_edge_before_container_close' ); ?>
</div>
<?php get_footer(); ?>
