<?php

if ( ! function_exists( 'sarto_core_reviews_map' ) ) {
	function sarto_core_reviews_map() {
		
		$reviews_panel = sarto_edge_add_admin_panel(
			array(
				'title' => esc_html__( 'Reviews', 'sarto-core' ),
				'name'  => 'panel_reviews',
				'page'  => '_page_page'
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'parent'      => $reviews_panel,
				'type'        => 'text',
				'name'        => 'reviews_section_title',
				'label'       => esc_html__( 'Reviews Section Title', 'sarto-core' ),
				'description' => esc_html__( 'Enter title that you want to show before average rating for each room', 'sarto-core' ),
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'parent'      => $reviews_panel,
				'type'        => 'textarea',
				'name'        => 'reviews_section_subtitle',
				'label'       => esc_html__( 'Reviews Section Subtitle', 'sarto-core' ),
				'description' => esc_html__( 'Enter subtitle that you want to show before average rating for each room', 'sarto-core' ),
			)
		);
	}
	
	add_action( 'sarto_edge_additional_page_options_map', 'sarto_core_reviews_map', 75 ); //one after elements
}