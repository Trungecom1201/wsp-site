<?php
namespace SartoCore\CPT\Shortcodes\Process;

use SartoCore\Lib;

class ProcessHolder implements Lib\ShortcodeInterface {
	private $base;

	public function __construct() {
		$this->base = 'edgtf_process_holder';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
			'name'                    => esc_html__('Process','sarto-core'),
			'base'                    => $this->getBase(),
			'as_parent'               => array('only' => 'edgtf_process_item'),
			'content_element'         => true,
			'category'                => esc_html__('by SARTO', 'sarto-core'),
			'icon'                    => 'icon-wpb-process extended-custom-icon',
			'js_view'                 => 'VcColumnView',
			'params'                  => array(
				array(
					'type'        => 'dropdown',
					'param_name'  => 'number_of_items',
					'heading'     => esc_html__('Number of Process Items', 'sarto-core'),
					'value'       => array(
						esc_html__('Three', 'sarto-core') => 'three',
						esc_html__('Four', 'sarto-core')  => 'four',
						esc_html__('Five', 'sarto-core')  => 'five'
					),
					'admin_label' => true,
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$default_atts = array(
			'number_of_items' => 'three'
		);

		$params            = shortcode_atts($default_atts, $atts);
		$params['content'] = $content;

		$params['holder_classes'] = array(
			'edgtf-process-holder',
			'edgtf-process-holder-items-'.$params['number_of_items']
		);

		return sarto_core_get_shortcode_module_template_part('templates/process-holder', 'process', '', $params);
	}
}