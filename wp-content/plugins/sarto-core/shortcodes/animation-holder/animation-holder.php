<?php
namespace SartoCore\CPT\Shortcodes\AnimationHolder;

use SartoCore\Lib;

class AnimationHolder implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_animation_holder';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'                    => esc_html__( 'Animation Holder', 'sarto-core' ),
					'base'                    => $this->base,
					"as_parent"               => array( 'except' => 'vc_row' ),
					'content_element'         => true,
					'category'                => esc_html__( 'by SARTO', 'sarto-core' ),
					'icon'                    => 'icon-wpb-animation-holder extended-custom-icon',
					'show_settings_on_create' => true,
					'js_view'                 => 'VcColumnView',
					'params'                  => array(
						array(
							'type'        => 'dropdown',
							'param_name'  => 'animation',
							'heading'     => esc_html__( 'Animation Type', 'sarto-core' ),
							'value'       => array(
								esc_html__( 'Element Grow In', 'sarto-core' )          => 'edgtf-grow-in',
								esc_html__( 'Element Fade In Down', 'sarto-core' )     => 'edgtf-fade-in-down',
								esc_html__( 'Element From Fade', 'sarto-core' )        => 'edgtf-element-from-fade',
								esc_html__( 'Element From Left', 'sarto-core' )        => 'edgtf-element-from-left',
								esc_html__( 'Element From Right', 'sarto-core' )       => 'edgtf-element-from-right',
								esc_html__( 'Element From Top', 'sarto-core' )         => 'edgtf-element-from-top',
								esc_html__( 'Element From Bottom', 'sarto-core' )      => 'edgtf-element-from-bottom',
								esc_html__( 'Element Flip In', 'sarto-core' )          => 'edgtf-flip-in',
								esc_html__( 'Element X Rotate', 'sarto-core' )         => 'edgtf-x-rotate',
								esc_html__( 'Element Z Rotate', 'sarto-core' )         => 'edgtf-z-rotate',
								esc_html__( 'Element Y Translate', 'sarto-core' )      => 'edgtf-y-translate',
								esc_html__( 'Element Fade In X Rotate', 'sarto-core' ) => 'edgtf-fade-in-left-x-rotate',
							),
							'save_always' => true
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'animation_delay',
							'heading'    => esc_html__( 'Animation Delay (ms)', 'sarto-core' )
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args = array(
			'animation'       => '',
			'animation_delay' => ''
		);
		extract( shortcode_atts( $args, $atts ) );
		
		$html = '<div class="edgtf-animation-holder ' . esc_attr( $animation ) . '" data-animation="' . esc_attr( $animation ) . '" data-animation-delay="' . esc_attr( $animation_delay ) . '"><div class="edgtf-animation-inner">' . do_shortcode( $content ) . '</div></div>';
		
		return $html;
	}
}