<div class="edgtf-clients-wc-holder <?php echo esc_attr($holder_classes); ?>">
	<div class="edgtf-ch-inner">
		<?php echo do_shortcode($content); ?>
	</div>
</div>