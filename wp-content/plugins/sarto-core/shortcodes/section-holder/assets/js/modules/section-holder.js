(function($) {
	'use strict';
	
	var sectionHolder = {};
	edgtf.modules.section_holder = sectionHolder;

    sectionHolder.edgtfInitSectionHolder = edgtfInitSectionHolder;


    sectionHolder.edgtfOnDocumentReady = edgtfOnDocumentReady;
	
	$(document).ready(edgtfOnDocumentReady);
    $(window).resize(edgtfOnWindowResize);
    $(window).load(edgtfOnWindowLoad);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function edgtfOnWindowLoad() {
        edgtfInitSectionHolder();
	}

    /*
     All functions to be called on $(document).ready() should be in this function
     */
    function edgtfOnDocumentReady() {
        edgtfInitSectionHolder();
        edgtfSectionHolderUncover();
    }

    /*
     All functions to be called on $(window).resize() should be in this function
     */
    function edgtfOnWindowResize() {
        edgtfInitSectionHolder();
    }
	

    /*
     **	Init Section Holder
     */

    function edgtfInitSectionHolder(){
        var sectionHolder = $('.edgtf-section-holder');
        if (sectionHolder.length){
            sectionHolder.each(function(){
                var thisHolder = $(this),
                    items = thisHolder.find('.edgtf-section-item'),
                    height = items.first().outerHeight();

                // for resize
                items.css('height', 'auto');

                items.each(function(){
                    var thisItem = $(this);

                    if (height < thisItem.outerHeight()){
                        height = thisItem.outerHeight();
                    }
                });

                items.each(function(){
                    var thisItem = $(this);

                    thisItem.css('height',height);
                });

                thisHolder.addClass('edgtf-appeared');
            });
        }
    }

    /*
     ** Section Holder uncover animation
     */
    function edgtfSectionHolderUncover() {
        var uncoverItems = $('.edgtf-section-holder.edgtf-sh-uncovering');

        if (uncoverItems.length) {
            var itemUncover = function() {
                uncoverItems.appear(function() {
                    $(this).addClass('edgtf-uncover');
                }, {accX: 0, accY: edgtfGlobalVars.vars.edgtfElementAppearAmount});
            }

            if (edgtf.body.hasClass('edgtf-content-switch-intro-on-page')) {
                $(document).on('edgtfCSILoaded', function() {
                    itemUncover();
                });
            } else {
                itemUncover();
            }
        }
    }
	
})(jQuery);