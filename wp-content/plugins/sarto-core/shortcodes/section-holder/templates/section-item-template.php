<div class="edgtf-section-item <?php echo esc_attr( $section_item_class ); ?>" <?php echo sarto_edge_get_inline_style( $section_item_style ); ?>>
	<?php
    if (( ! empty( $custom_icon ) || ! empty ( $icon_pack )) && empty( $hover_image )) { ?>
        <div class="edgtf-section-item-overlay" <?php echo sarto_edge_get_inline_style( $section_item_overlay_style ); ?>>
            <div class="edgtf-section-item-overlay-inner-holder">
                <div class="edgtf-section-item-overlay-inner">
					<?php
					if ( ! empty( $custom_icon ) ) :
						echo wp_get_attachment_image( $custom_icon, 'full' );
                    elseif ( $icon_pack ) :
						echo sarto_core_get_shortcode_module_template_part( 'templates/icon', 'icon-with-text', '', array( 'icon_parameters' => $icon_parameters ) );
					endif;
					?>
                </div>
            </div>
        </div>
	<?php } ?>
    <div class="edgtf-section-item-inner">
        <div class="edgtf-section-item-content">
			<?php echo do_shortcode( $content ); ?>
        </div>
    </div>
    <?php if (!empty($hover_image)) :
        $hover_style = "background-image: url(" . wp_get_attachment_url( $hover_image ) . ");"; 
    ?>
        <div class="edgtf-si-hover-image" <?php echo sarto_edge_get_inline_style( $hover_style ); ?>></div>
    <?php endif; ?>
</div>