<?php

namespace SartoCore\CPT\Shortcodes\SectionHolder;

use SartoCore\Lib;

class SectionHolder implements Lib\ShortcodeInterface {
	private $base;

	function __construct() {
		$this->base = 'edgtf_section_holder';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map( array(
				'name'      => esc_html__( 'Section Holder', 'sarto-core' ),
				'base'      => $this->base,
				'icon'      => 'icon-wpb-section-holder extended-custom-icon',
				'category'  => esc_html__( 'by SARTO', 'sarto-core' ),
				'as_parent' => array( 'only' => 'edgtf_section_item' ),
				'js_view'   => 'VcColumnView',
				'params'    => array(
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Number of Items', 'sarto-core' ),
						'admin_label' => true,
						'param_name'  => 'number_of_items',
						'save_always' => true,
						'value'       => array(
							'4' => '4',
							'1' => '1',
							'2' => '2',
							'6' => '6',
						)
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Title Area Background Color', 'sarto-core' ),
						'param_name' => 'title_area_background_color',
						'group'      => esc_html__( 'Design Options', 'sarto-core' )
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'title',
						'heading'     => esc_html__('Title', 'sarto-core'),
						'admin_label' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'title_underscore',
						'heading'     => esc_html__('Enable SVG Underscore below Title', 'sarto-core'),
						'value'       => array_flip(sarto_edge_get_yes_no_select_array(false, true)),
						'save_always' => true,
						'group'       => esc_html__('Title Style', 'sarto-core')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'title_underscore_size',
						'heading'     => esc_html__('SVG Underscore Size', 'sarto-core'),
						'value'       => array(
							esc_html__('Normal', 'sarto-core') => 'normal',
							esc_html__('Large', 'sarto-core')  => 'large'
						),
						'save_always' => true,
						'dependency' => array('element' => 'title_underscore', 'value' => array('yes')),
						'group'       => esc_html__('Title Style', 'sarto-core')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'title_tag',
						'heading'     => esc_html__('Title Tag', 'sarto-core'),
						'value'       => array_flip(sarto_edge_get_title_tag(true)),
						'save_always' => true,
						'dependency'  => array('element' => 'title', 'not_empty' => true),
						'group'       => esc_html__('Title Style', 'sarto-core')
					),
					array(
						'type'       => 'colorpicker',
						'param_name' => 'title_color',
						'heading'    => esc_html__('Title Color', 'sarto-core'),
						'dependency' => array('element' => 'title', 'not_empty' => true),
						'group'      => esc_html__('Title Style', 'sarto-core')
					),
					array(
						'type'       => 'textarea',
						'param_name' => 'subtitle',
						'heading'    => esc_html__('Subtitle', 'sarto-core')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'subtitle_tag',
						'heading'     => esc_html__('Subtitle Tag', 'sarto-core'),
						'value'       => array_flip(sarto_edge_get_title_tag(true, array(
							'p'    => 'p',
							'span' => 'span'
						))),
						'save_always' => true,
						'dependency'  => array('element' => 'subtitle', 'not_empty' => true),
						'group'       => esc_html__('Subtitle Style', 'sarto-core')
					),
					array(
						'type'       => 'colorpicker',
						'param_name' => 'subtitle_color',
						'heading'    => esc_html__('Subtitle Color', 'sarto-core'),
						'dependency' => array('element' => 'subtitle', 'not_empty' => true),
						'group'      => esc_html__('Subtitle Style', 'sarto-core')
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'subtitle_font_size',
						'heading'    => esc_html__('Subtitle Font Size (px)', 'sarto-core'),
						'dependency' => array('element' => 'subtitle', 'not_empty' => true),
						'group'      => esc_html__('Subtitle Style', 'sarto-core')
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'subtitle_line_height',
						'heading'    => esc_html__('Subtitle Line Height (px)', 'sarto-core'),
						'dependency' => array('element' => 'subtitle', 'not_empty' => true),
						'group'      => esc_html__('Subtitle Style', 'sarto-core')
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'subtitle_font_weight',
						'heading'     => esc_html__('Subtitle Font Weight', 'sarto-core'),
						'value'       => array_flip(sarto_edge_get_font_weight_array(true)),
						'save_always' => true,
						'dependency'  => array('element' => 'subtitle', 'not_empty' => true),
						'group'       => esc_html__('Subtitle Style', 'sarto-core')
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'subtitle_margin',
						'heading'    => esc_html__('Subtitle Top Margin (px)', 'sarto-core'),
						'dependency' => array('element' => 'subtitle', 'not_empty' => true),
						'group'      => esc_html__('Subtitle Style', 'sarto-core')
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Show Border Between Items', 'sarto-core' ),
						'param_name' => 'show_border',
						'value'      => array(
							esc_html__( 'Yes', 'sarto-core' ) => 'yes',
							esc_html__( 'No', 'sarto-core' )  => 'no'
						)
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'uncovering_animation',
						'heading'    => esc_html__( 'Enable Uncovering Animation for Title Area', 'sarto-core' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array(false) ),
					),
                	array(
                		'type'       => 'colorpicker',
                		'param_name' => 'first_mask_color',
                		'heading'    => esc_html__( 'First Mask Color', 'sarto-core' ),
						'dependency'  => array( 'element' => 'uncovering_animation', 'value' => array( 'yes' ) )
                	),
                	array(
                		'type'       => 'colorpicker',
                		'param_name' => 'second_mask_color',
                		'heading'    => esc_html__( 'Second Mask Color', 'sarto-core' ),
						'dependency'  => array( 'element' => 'uncovering_animation', 'value' => array( 'yes' ) )
                	),
				)
			) );
		}
	}

	public function render( $atts, $content = null ) {

		$args              = array(
			'number_of_items'               => '4',
			'title_area_background_color'   => '',
			'title'                         => '',
			'title_underscore'              => '',
			'title_underscore_size'         => '',
			'title_tag'                     => '',
			'title_color'                   => '',
			'subtitle'                      => '',
			'subtitle_tag'                  => '',
			'subtitle_color'                => '',
			'subtitle_font_size'            => '',
			'subtitle_line_height'          => '',
			'subtitle_font_weight'          => '',
			'subtitle_margin'               => '',
			'show_border'                   => 'yes',
			'uncovering_animation'			=> '',
			'first_mask_color'				=> '',
			'second_mask_color'				=> ''
		);
		$params            = shortcode_atts( $args, $atts );
		$params['content'] = $content;

		$params['section_classes']  = $this->getSectionClasses( $params );
		$params['title_area_style'] = $this->getSectionTitleAreaStyle( $params );
		$params['title_params']     = $this->getTitleSectionParams( $params );
		$params['mask_styles'] 		 = $this->getMaskStyles( $params );

		$html = sarto_core_get_shortcode_module_template_part( 'templates/section-holder-template', 'section-holder', '', $params );

		return $html;

	}

	public function getSectionClasses( $params ) {
		$classes   = array();
		$classes[] = 'edgtf-section-holder';

		switch ( $params['number_of_items'] ) {
			case '1':
				$classes[] = 'edgtf-sh-items-one';
				break;
			case '2':
				$classes[] = 'edgtf-sh-items-two';
				break;
			case '6':
				$classes[] = 'edgtf-sh-items-six';
				break;
			default:
				$classes[] = 'edgtf-sh-items-four';
				break;
		}

		if ( $params['show_border'] == 'yes' ) {
			$classes[] = 'edgtf-sh-border';
		}

		if ( $params['uncovering_animation'] == 'yes' ) {
			$classes[] = 'edgtf-sh-uncovering';
		}

		return implode( ' ', $classes );
	}

	public function getSectionTitleAreaStyle( $params ) {
		$style = array();

		if ( $params['title_area_background_color'] !== '' ) {
			$style[] = 'background-color:' . $params['title_area_background_color'];
		}

		return implode( '; ', $style );
	}

	public function getTitleSectionParams( $params ) {
		$title_params = array();

		if ( $params['title'] !== '' ) {
			$title_params['title'] = $params['title'];
		}

		if ( $params['title_underscore'] !== '' ) {
			$title_params['title_underscore'] = $params['title_underscore'];
		}

		if ( $params['title_underscore_size'] !== '' ) {
			$title_params['title_underscore_size'] = $params['title_underscore_size'];
		}

		if ( $params['title_tag'] !== '' ) {
			$title_params['title_tag'] = $params['title_tag'];
		}

		if ( $params['title_color'] !== '' ) {
			$title_params['title_color'] = $params['title_color'];
		}

		if ( $params['subtitle'] !== '' ) {
			$title_params['subtitle'] = $params['subtitle'];
		}

		if ( $params['subtitle_tag'] !== '' ) {
			$title_params['subtitle_tag'] = $params['subtitle_tag'];
		}

		if ( $params['subtitle_color'] !== '' ) {
			$title_params['subtitle_color'] = $params['subtitle_color'];
		}

		if ( $params['subtitle_font_size'] !== '' ) {
			$title_params['subtitle_font_size'] = $params['subtitle_font_size'];
		}

		if ( $params['subtitle_line_height'] !== '' ) {
			$title_params['subtitle_line_height'] = $params['subtitle_line_height'];
		}

		if ( $params['subtitle_font_weight'] !== '' ) {
			$title_params['subtitle_font_weight'] = $params['subtitle_font_weight'];
		}

		if ( $params['subtitle_margin'] !== '' ) {
			$title_params['subtitle_margin'] = $params['subtitle_margin'];
		}

		return $title_params;
	}

	private function getMaskStyles( $params ) {
		$styles = array();
		$styles['first'] = '';
		$styles['second'] = '';
		
		if ( ! empty( $params['first_mask_color'] ) ) {
			$styles['first'] = 'background-color: ' . $params['first_mask_color'];
		}

		if ( ! empty( $params['second_mask_color'] ) ) {
			$styles['second'] = 'background-color: ' . $params['second_mask_color'];
		}
		
		return $styles;
	}
}
