<?php
namespace SartoCore\CPT\Shortcodes\ContentSwitchIntro;

use SartoCore\Lib;

class ContentSwitchIntro implements Lib\ShortcodeInterface {
	private $base;
	
	public function __construct() {
		$this->base = 'edgtf_content_switch_intro';
		
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Content Switch Intro', 'sarto-core' ),
					'base'                      => $this->getBase(),
					'category'                  => esc_html__( 'by SARTO', 'sarto-core' ),
					'icon'                      => 'icon-wpb-content-switch-intro extended-custom-icon',
					'allowed_container_element' => 'vc_row',
					'params'                    => array(
                        array(
                            'type' => 'param_group',
                            'heading' => esc_html__( 'Items', 'sarto-core' ),
                            'param_name' => 'items',
                            'params' => array(
                            	array(
                            		'type'        => 'dropdown',
                            		'param_name'  => 'item_type',
                            		'heading'     => esc_html__( 'Item Type', 'sarto-core' ),
                            		'value'       => array(
                            			esc_html__( 'Image', 'sarto-core' ) => 'image',
                            			esc_html__( 'Video', 'sarto-core' ) => 'video'
                            		),
                            		'save_always' => true,
                            		'admin_label' => true
                            	),
                            	array(
                            	    'type'        => 'attach_image',
                            	    'param_name'  => 'item_image',
                            	    'heading'     => esc_html__( 'Item Image', 'sarto-core' ),
									'dependency'  => array( 'element' => 'item_type', 'value' => array( 'image' ) )
                            	),
	                        	array(
	                        	    'type'        => 'attach_image',
	                        	    'param_name'  => 'additional_image_layer',
	                        	    'heading'     => esc_html__( 'Additional Image Layer', 'sarto-core' ),
									'dependency'  => array( 'element' => 'item_image', 'not_empty' => true )
	                        	),
                                array(
                                    'type'       => 'textfield',
                                    'param_name' => 'item_video_url',
                                    'heading'    => esc_html__( 'Item Video Url', 'sarto-core' ),
									'dependency'  => array( 'element' => 'item_type', 'value' => array( 'video' ) )
                                ),
                            ),
							'group'      => esc_html__( 'Items', 'sarto-core' )
                        ),
						array(
							'type'        => 'textfield',
							'param_name'  => 'preloading_message',
							'heading'     => esc_html__( 'Preloading Message', 'sarto-core' ),
                    		'admin_label' => true,
							'group'      => esc_html__( 'Text Content', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'sarto-core' ),
                    		'admin_label' => true,
							'group'      => esc_html__( 'Text Content', 'sarto-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'title_tag',
							'heading'     => esc_html__( 'Title Tag', 'sarto-core' ),
							'value'       => array_flip( sarto_edge_get_title_tag( true ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'title', 'not_empty' => true ),
							'group'      => esc_html__( 'Text Content', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'subtitle',
							'heading'     => esc_html__( 'Subtitle', 'sarto-core' ),
							'group'      => esc_html__( 'Text Content', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'subtitle_font_size',
							'heading'     => esc_html__( 'Subtitle Font Size', 'sarto-core' ),
							'dependency'  => array( 'element' => 'subtitle', 'not_empty' => true ),
							'group'      => esc_html__( 'Text Content', 'sarto-core' )
						),
						array(
							'type'       => 'dropdown',
							'param_name' => 'enable_separator',
							'heading'    => esc_html__( 'Enable Separator', 'sarto-core' ),
							'value'      => array_flip( sarto_edge_get_yes_no_select_array( false ) ),
							'group'      => esc_html__( 'Text Content', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'button_text',
							'heading'     => esc_html__( 'Button Text', 'sarto-core' ),
							'group'      => esc_html__( 'Text Content', 'sarto-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'button_link',
							'heading'    => esc_html__( 'Button Link', 'sarto-core' ),
							'dependency' => array( 'element' => 'button_text', 'not_empty' => true ),
							'group'      => esc_html__( 'Text Content', 'sarto-core' )
						),
						array(
							'type'       => 'dropdown',
							'param_name' => 'button_target',
							'heading'    => esc_html__( 'Button Link Target', 'sarto-core' ),
							'value'      => array_flip( sarto_edge_get_link_target_array() ),
							'dependency' => array( 'element' => 'button_link', 'not_empty' => true ),
							'group'      => esc_html__( 'Text Content', 'sarto-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'background_color',
							'heading'    => esc_html__( 'Background Color', 'sarto-core' ),
							'group'      => esc_html__( 'Design', 'sarto-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'first_mask_color',
							'heading'    => esc_html__( 'First Mask Color', 'sarto-core' ),
							'group'      => esc_html__( 'Design', 'sarto-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'second_mask_color',
							'heading'    => esc_html__( 'Second Mask Color', 'sarto-core' ),
							'group'      => esc_html__( 'Design', 'sarto-core' )
						),
						array(
							'type'       => 'attach_image',
							'param_name' => 'static_layer',
							'heading'    => esc_html__( 'Static Layer', 'sarto-core' ),
							'group'      => esc_html__( 'Design', 'sarto-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'text_color',
							'heading'    => esc_html__( 'Text Color', 'sarto-core' ),
							'group'      => esc_html__( 'Design', 'sarto-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'separator_color',
							'heading'    => esc_html__( 'Separator Color', 'sarto-core' ),
							'dependency' => array( 'element' => 'enable_separator', 'value' => array('yes') ),
							'group'      => esc_html__( 'Design', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'separator_width',
							'heading'     => esc_html__( 'Separator Width', 'sarto-core' ),
							'dependency' => array( 'element' => 'enable_separator', 'value' => array('yes') ),
							'group'      => esc_html__( 'Design', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'separator_height',
							'heading'     => esc_html__( 'Separator Height', 'sarto-core' ),
							'dependency' => array( 'element' => 'enable_separator', 'value' => array('yes') ),
							'group'      => esc_html__( 'Design', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'separator_margins',
							'heading'     => esc_html__( 'Separator Margins', 'sarto-core' ),
							'dependency'  => array( 'element' => 'enable_separator', 'value' => array('yes') ),
							'description' => esc_html__( 'Enter px based margin values for top, right, bottom and left margin respectively.', 'sarto-core' ),
							'group'       => esc_html__( 'Design', 'sarto-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'layout',
							'heading'     => esc_html__( 'Layout', 'sarto-core' ),
							'value'       => array(
								esc_html__( 'Centered', 'sarto-core' ) => 'centered',
								esc_html__( 'Left Aligned', 'sarto-core' ) => 'left-aligned'
							),
							'save_always' => true,
							'group'      => esc_html__( 'Layout and Behavior', 'sarto-core' )
						),
						array(
							'type'       => 'dropdown',
							'param_name' => 'fix_mask',
							'heading'    => esc_html__( 'Fix mask', 'sarto-core' ),
							'dependency' => array( 'element' => 'layout', 'value' => array('left-aligned') ),
							'value'      => array_flip( sarto_edge_get_yes_no_select_array( false ) ), 
							'description' => esc_html__( 'Enabling this option will set mask at 33% shortcode width upon inital animation.', 'sarto-core' ),
							'group'      => esc_html__( 'Layout and Behavior', 'sarto-core' )
						),
						array(
							'type'       => 'dropdown',
							'param_name' => 'enable_nav',
							'heading'    => esc_html__( 'Enable Nav', 'sarto-core' ),
							'value'      => array_flip( sarto_edge_get_yes_no_select_array( false ) ),
							'group'      => esc_html__( 'Layout and Behavior', 'sarto-core' )
						),
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'items'                  	=> '',
			'preloading_message'       	=> '',
			'title'                  	=> '',
			'title_tag'           		=> '',
			'subtitle'              	=> '',
			'subtitle_font_size'       	=> '',
			'enable_separator'         	=> '',
			'button_text'               => '',
			'button_link'               => '',
			'button_target'            	=> '_self',
			'background_color'         	=> '',
			'first_mask_color'         	=> '',
			'second_mask_color'        	=> '',
			'static_layer'             	=> '',
			'text_color'            	=> '',
			'separator_color'    		=> '',
			'separator_width'    		=> '',
			'separator_height'    		=> '',
			'separator_margins'    		=> '',
			'layout'	 				=> 'centered',
			'fix_mask'		            => '',
			'enable_nav'     			=> ''
		);
		$params = shortcode_atts( $args, $atts );
		
        $params['content'] = $content;
        $params['items'] = json_decode( urldecode( $params['items'] ), true );
		$params['holder_classes'] = $this->getHolderClasses( $params );
		$params['holder_styles'] = $this->getHolderStyles( $params );
		$params['button_params'] = $this->getButtonParameters( $params );
		$params['text_styles'] = $this->getTextStyles( $params );
		$params['subtitle_styles'] = $this->getSubtitleStyles( $params );
		$params['separator_styles'] = $this->getSeparatorStyles( $params );
		$params['separator_size'] = $this->getSeparatorSize( $params );
		$params['mask_styles'] = $this->getMaskStyles( $params );

		$html = sarto_core_get_shortcode_module_template_part( 'templates/content-switch-intro-template', 'content-switch-intro', '', $params );
		
		return $html;
	}
	
	private function getHolderClasses( $params ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['layout'] ) ? 'edgtf-csi-'.esc_attr( $params['layout'] ) : '';
		$holderClasses[] = ! empty( $params['static_layer'] ) ? 'edgtf-csi-with-static-layer' : '';
		$holderClasses[] = ! empty( $params['preloading_message'] ) ? 'edgtf-csi-with-preloading-msg' : '';
		$holderClasses[] = empty( $params['title_tag'] ) ? 'edgtf-csi-with-hero-title' : '';
		$holderClasses[] = $params['enable_separator'] === 'yes' ? 'edgtf-csi-with-separator' : '';
		$holderClasses[] = $params['fix_mask'] === 'yes' ? 'edgtf-csi-fixed-mask' : '';
		$holderClasses[] = $params['enable_nav'] === 'yes' ? 'edgtf-csi-with-nav' : '';
		
		return implode( ' ', $holderClasses );
	}

	private function getHolderStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['background_color'] ) ) {
			$styles[] = 'background-color: ' . $params['background_color'];
		}
		
		return implode( ';', $styles );
	}
	
	private function getTextStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['text_color'] ) ) {
			$styles[] = 'color: ' . $params['text_color'];
		}
		
		return implode( ';', $styles );
	}

	private function getSubtitleStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['subtitle_font_size'] ) ) {
			$styles[] = 'font-size: ' .sarto_edge_filter_px($params['subtitle_font_size']) . 'px';
		}
		
		return implode( ';', $styles );
	}

	private function getSeparatorStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['separator_color'] ) ) {
			$styles[] = 'fill: ' . $params['separator_color'];
		}

		if ( ! empty( $params['separator_margins'] ) ) {
			$styles[] = 'margin: '  .sarto_edge_filter_px( $params['separator_margins'] );
		}
		
		return implode( ';', $styles );
	}

	private function getSeparatorSize( $params ) {
		$styles = array();
		
		if ( ! empty( $params['separator_width'] ) ) {
			$styles['width'] = sarto_edge_filter_px( $params['separator_width'] );
		} else {
			$styles['width'] = 109;
		}

		if ( ! empty( $params['separator_height'] ) ) {
			$styles['height'] = sarto_edge_filter_px( $params['separator_height'] );
		} else {
			$styles['height'] = 12;
		}

		return $styles;
	}

	private function getButtonParameters( $params ) {
		$button_params_array = array();
		
		if ( ! empty( $params['button_text'] ) ) {
			$button_params_array['text'] = $params['button_text'];
		}
		
		if ( ! empty( $params['button_link'] ) ) {
			$button_params_array['link'] = $params['button_link'];
		}

		$button_params_array['target'] = ! empty( $params['button_target'] ) ? $params['button_target'] : '_self';

		$button_params_array['type'] = 'outline';
		$button_params_array['size'] = 'medium';
		$button_params_array['custom_class'] = 'edgtf-csi-btn';
		if ( ! empty( $params['text_color'] ) ) {
			$button_params_array['color'] = $params['text_color'];
			$button_params_array['border_color'] = $params['text_color'];
			$button_params_array['hover_color'] = $params['text_color'];
		}

		$button_params_array['hover_background_color'] = '#bdab85';
		$button_params_array['hover_border_color'] = '#bdab85';

		return $button_params_array;
	}

	private function getMaskStyles( $params ) {
		$styles = array();
		$styles['first'] = '';
		$styles['second'] = '';
		
		if ( ! empty( $params['first_mask_color'] ) ) {
			$styles['first'] = 'background-color: ' . $params['first_mask_color'];
		}

		if ( ! empty( $params['second_mask_color'] ) ) {
			$styles['second'] = 'background-color: ' . $params['second_mask_color'];
		}
		
		return $styles;
	}
}