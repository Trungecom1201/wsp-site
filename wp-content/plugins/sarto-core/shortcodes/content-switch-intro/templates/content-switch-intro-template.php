<div id="edgtf-content-switch-intro" <?php sarto_edge_class_attribute( $holder_classes ); ?> <?php echo sarto_edge_get_inline_style( $holder_styles ); ?>>
	<?php if (!empty($preloading_message)) : ?>
		<h2 class="edgtf-csi-msg" <?php echo sarto_edge_get_inline_style( $text_styles ); ?>><?php echo esc_html( $preloading_message ); ?></h2>
	<?php endif; ?>
	<div class="edgtf-csi-cover"  <?php echo sarto_edge_get_inline_style( $mask_styles['first'] ); ?>></div>
	<?php if ( !empty( $items ) ) : ?>
		<div class="edgtf-csi-items-holder">
			<div class="swiper-container">
			    <div class="swiper-wrapper">
					<?php $i = 1; ?>
					<?php foreach ( $items as $item ) : ?>
			        	<div class="edgtf-csi-item swiper-slide" data-index="<?php echo esc_attr($i); ?>">
			            	<?php if ( $item['item_type'] == 'image') : 
						    	$bgrnd_img_style = '';
						    	$aux_img_style = '';

								if (!empty( $item['item_image']) ) {
						    		$bgrnd_img_style = "background-image: url(" . wp_get_attachment_url( $item['item_image'] ) . ");"; 
						    	}

				    			if (!empty( $item['additional_image_layer']) ) {
				    	    		$aux_img_style = "background-image: url(" . wp_get_attachment_url( $item['additional_image_layer'] ) . ");"; 
				    	    	}
			        		?>
			        			<div class="edgtf-csi-item-image" <?php echo sarto_edge_get_inline_style( $bgrnd_img_style ); ?>></div>
			        			<div class="edgtf-csi-item-aux-image" <?php echo sarto_edge_get_inline_style( $aux_img_style ); ?> data-swiper-parallax="-80%"></div>
							<?php endif; ?>
			            	<?php if ( $item['item_type'] == 'video' ) : 
						    	$video_link = '';

								if ( !empty( $item['item_video_url'] ) ) {
						    		$video_link = $item['item_video_url']; 
						    	}
			        		?>
			        			<video autoplay loop class="edgtf-csi-item-video">
			        			  	<source src="<?php echo esc_url( $video_link ); ?>" type="video/mp4">
			        			</video>
							<?php endif; ?>
			        	</div>
						<?php $i++; ?>
					<?php endforeach; ?>
				</div>
			</div>
			<?php if (!empty($static_layer)) : 
			    $static_layer_style = "background-image: url(" . wp_get_attachment_url( $static_layer ) . ");"; 
	    	?>
    			<div class="edgtf-csi-static-layer" <?php echo sarto_edge_get_inline_style( $static_layer_style ); ?>></div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<?php if ( $enable_nav == 'yes' && sizeof($items) > 1 ) : ?>
		<span class="edgtf-csi-nav-el edgtf-csi-prev"></span>
		<span class="edgtf-csi-nav-el edgtf-csi-next"></span>
	<?php endif; ?>
	<div class="edgtf-csi-content-holder">
		<div class="edgtf-csi-content-inner" <?php echo sarto_edge_get_inline_style( $text_styles ); ?>>
	        <?php if ( !empty( $title ) ) : ?>
	            <div class="edgtf-csi-title-holder">
	            	<?php if ( empty( $title_tag ) ) : $title_tag = 'h1'; endif; ?>
	            	<<?php echo esc_attr( $title_tag ); ?> class="edgtf-csi-title"><?php echo esc_html( $title ); ?></<?php echo esc_attr( $title_tag ); ?>>
    	        </div>
	        <?php endif; ?>
	        <?php if ( $enable_separator == 'yes' ) : ?>
				<div class="edgtf-csi-separator-holder" <?php echo sarto_edge_get_inline_style( $separator_styles ); ?>>
		           <?php echo sarto_edge_get_title_underscore_svg_image( $separator_size['width'], $separator_size['height'], '', 'none' ); ?>
				</div>
	        <?php endif; ?>
			<?php if ( !empty( $subtitle ) ) : ?>
				<div class="edgtf-csi-subtitle-holder">
		            <p class="edgtf-csi-subtitle" <?php echo sarto_edge_get_inline_style( $subtitle_styles ); ?>><?php echo esc_html( $subtitle ); ?></p>
				</div>
	        <?php endif; ?>
    		<?php if ( !empty( $button_link ) ) : ?>
        		<div class="edgtf-csi-btn-holder">
					<?php echo sarto_edge_get_button_html( $button_params ); ?>
        		</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="edgtf-csi-mask edgtf-csi-mask-intro"  <?php echo sarto_edge_get_inline_style( $mask_styles['first'] ); ?>></div>
	<div class="edgtf-csi-mask edgtf-csi-mask-default" <?php echo sarto_edge_get_inline_style( $mask_styles['second'] ); ?>></div>
	<div class="edgtf-csi-mask edgtf-csi-mask-aux"  <?php echo sarto_edge_get_inline_style( $mask_styles['first'] ); ?>></div>
</div>