<?php
if ( ! function_exists( 'sarto_core_add_content_crossfade_images' ) ) {
	function sarto_core_add_content_crossfade_images( $shortcodes_class_name ) {
		$shortcodes = array(
			'SartoCore\CPT\Shortcodes\CrossfadeImages\CrossfadeImages'
		);
		
		$shortcodes_class_name = array_merge( $shortcodes_class_name, $shortcodes );
		
		return $shortcodes_class_name;
	}
	
	add_filter( 'sarto_core_filter_add_vc_shortcode', 'sarto_core_add_content_crossfade_images' );
}

if ( ! function_exists( 'sarto_core_set_content_crossfade_images_icon_class_name_for_vc_shortcodes' ) ) {
	/**
	 * Function that set custom icon class name for content crossfade images shortcode to set our icon for Visual Composer shortcodes panel
	 */
	function sarto_core_set_content_crossfade_images_icon_class_name_for_vc_shortcodes( $shortcodes_icon_class_array ) {
		$shortcodes_icon_class_array[] = '.icon-wpb-crossafade-images';
		
		return $shortcodes_icon_class_array;
	}
	
	add_filter( 'sarto_core_filter_add_vc_shortcodes_custom_icon_class', 'sarto_core_set_content_crossfade_images_icon_class_name_for_vc_shortcodes' );
}