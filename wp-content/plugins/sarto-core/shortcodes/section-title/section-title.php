<?php
namespace SartoCore\CPT\Shortcodes\SectionTitle;

use SartoCore\Lib;

class SectionTitle implements Lib\ShortcodeInterface
{
    private $base;

    function __construct() {
        $this->base = 'edgtf_section_title';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        if (function_exists('vc_map')) {
            vc_map(
                array(
                    'name'                      => esc_html__('Section Title', 'sarto-core'),
                    'base'                      => $this->base,
                    'category'                  => esc_html__('by SARTO', 'sarto-core'),
                    'icon'                      => 'icon-wpb-section-title extended-custom-icon',
                    'allowed_container_element' => 'vc_row',
                    'params'                    => array(
                        array(
                            'type'        => 'textfield',
                            'param_name'  => 'custom_class',
                            'heading'     => esc_html__('Custom CSS Class', 'sarto-core'),
                            'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS', 'sarto-core')
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'type',
                            'heading'     => esc_html__('Type', 'sarto-core'),
                            'value'       => array(
                                esc_html__('Standard', 'sarto-core')    => 'standard',
                                esc_html__('Two Columns', 'sarto-core') => 'two-columns'
                            ),
                            'save_always' => true
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'title_position',
                            'heading'     => esc_html__('Title - Text Position', 'sarto-core'),
                            'value'       => array(
                                esc_html__('Title Left - Text Right', 'sarto-core') => 'title-left',
                                esc_html__('Title Right - Text Left', 'sarto-core') => 'title-right'
                            ),
                            'save_always' => true,
                            'dependency'  => array('element' => 'type', 'value' => array('two-columns'))
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'columns_space',
                            'heading'     => esc_html__('Space Between Columns', 'sarto-core'),
                            'value'       => array(
                                esc_html__('Normal', 'sarto-core') => 'normal',
                                esc_html__('Small', 'sarto-core')  => 'small',
                                esc_html__('Tiny', 'sarto-core')   => 'tiny'
                            ),
                            'save_always' => true,
                            'dependency'  => array('element' => 'type', 'value' => array('two-columns'))
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'position',
                            'heading'     => esc_html__('Horizontal Position', 'sarto-core'),
                            'value'       => array(
                                esc_html__('Default', 'sarto-core') => '',
                                esc_html__('Left', 'sarto-core')    => 'left',
                                esc_html__('Center', 'sarto-core')  => 'center',
                                esc_html__('Right', 'sarto-core')   => 'right'
                            ),
                            'save_always' => true,
                            'dependency'  => array('element' => 'type', 'value' => array('standard'))
                        ),
                        array(
                            'type'       => 'textfield',
                            'param_name' => 'holder_padding',
                            'heading'    => esc_html__('Holder Side Padding (px or %)', 'sarto-core')
                        ),
                        array(
                            'type'        => 'textfield',
                            'param_name'  => 'title',
                            'heading'     => esc_html__('Title', 'sarto-core'),
                            'admin_label' => true
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'title_underscore',
                            'heading'     => esc_html__('Enable SVG Underscore below Title', 'sarto-core'),
                            'value'       => array_flip(sarto_edge_get_yes_no_select_array(false, true)),
                            'save_always' => true,
                            'group'       => esc_html__('Title Style', 'sarto-core')
                        ),
	                    array(
		                    'type'        => 'dropdown',
		                    'param_name'  => 'title_underscore_size',
		                    'heading'     => esc_html__('SVG Underscore Size', 'sarto-core'),
		                    'value'       => array(
			                    esc_html__('Normal', 'sarto-core') => 'normal',
			                    esc_html__('Large', 'sarto-core')  => 'large'
		                    ),
		                    'save_always' => true,
		                    'dependency' => array('element' => 'title_underscore', 'not_empty' => true),
		                    'group'       => esc_html__('Title Style', 'sarto-core')
	                    ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'title_tag',
                            'heading'     => esc_html__('Title Tag', 'sarto-core'),
                            'value'       => array_flip(sarto_edge_get_title_tag(true)),
                            'save_always' => true,
                            'dependency'  => array('element' => 'title', 'not_empty' => true),
                            'group'       => esc_html__('Title Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'colorpicker',
                            'param_name' => 'title_color',
                            'heading'    => esc_html__('Title Color', 'sarto-core'),
                            'dependency' => array('element' => 'title', 'not_empty' => true),
                            'group'      => esc_html__('Title Style', 'sarto-core')
                        ),
                        array(
                            'type'        => 'textfield',
                            'param_name'  => 'title_bold_words',
                            'heading'     => esc_html__('Words with Bold Font Weight', 'sarto-core'),
                            'description' => esc_html__('Enter the positions of the words you would like to display in a "bold" font weight. Separate the positions with commas (e.g. if you would like the first, second, and third word to have a light font weight, you would enter "1,2,3")', 'sarto-core'),
                            'dependency'  => array('element' => 'title', 'not_empty' => true),
                            'group'       => esc_html__('Title Style', 'sarto-core')
                        ),
                        array(
                            'type'        => 'textfield',
                            'param_name'  => 'title_light_words',
                            'heading'     => esc_html__('Words with Light Font Weight', 'sarto-core'),
                            'description' => esc_html__('Enter the positions of the words you would like to display in a "light" font weight. Separate the positions with commas (e.g. if you would like the first, third, and fourth word to have a light font weight, you would enter "1,3,4")', 'sarto-core'),
                            'dependency'  => array('element' => 'title', 'not_empty' => true),
                            'group'       => esc_html__('Title Style', 'sarto-core')
                        ),
                        array(
                            'type'        => 'textfield',
                            'param_name'  => 'title_break_words',
                            'heading'     => esc_html__('Position of Line Break', 'sarto-core'),
                            'description' => esc_html__('Enter the position of the word after which you would like to create a line break (e.g. if you would like the line break after the 3rd word, you would enter "3")', 'sarto-core'),
                            'dependency'  => array('element' => 'title', 'not_empty' => true),
                            'group'       => esc_html__('Title Style', 'sarto-core')
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'disable_break_words',
                            'heading'     => esc_html__('Disable Line Break for Smaller Screens', 'sarto-core'),
                            'value'       => array_flip(sarto_edge_get_yes_no_select_array(false)),
                            'save_always' => true,
                            'dependency'  => array('element' => 'title', 'not_empty' => true),
                            'group'       => esc_html__('Title Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'textarea',
                            'param_name' => 'subtitle',
                            'heading'    => esc_html__('Subtitle', 'sarto-core')
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'subtitle_tag',
                            'heading'     => esc_html__('Subtitle Tag', 'sarto-core'),
                            'value'       => array_flip(sarto_edge_get_title_tag(true, array(
                                'p'    => 'p',
                                'span' => 'span'
                            ))),
                            'save_always' => true,
                            'dependency'  => array('element' => 'subtitle', 'not_empty' => true),
                            'group'       => esc_html__('Subtitle Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'colorpicker',
                            'param_name' => 'subtitle_color',
                            'heading'    => esc_html__('Subtitle Color', 'sarto-core'),
                            'dependency' => array('element' => 'subtitle', 'not_empty' => true),
                            'group'      => esc_html__('Subtitle Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'textfield',
                            'param_name' => 'subtitle_font_size',
                            'heading'    => esc_html__('Subtitle Font Size (px)', 'sarto-core'),
                            'dependency' => array('element' => 'subtitle', 'not_empty' => true),
                            'group'      => esc_html__('Subtitle Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'textfield',
                            'param_name' => 'subtitle_line_height',
                            'heading'    => esc_html__('Subtitle Line Height (px)', 'sarto-core'),
                            'dependency' => array('element' => 'subtitle', 'not_empty' => true),
                            'group'      => esc_html__('Subtitle Style', 'sarto-core')
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'subtitle_font_weight',
                            'heading'     => esc_html__('Subtitle Font Weight', 'sarto-core'),
                            'value'       => array_flip(sarto_edge_get_font_weight_array(true)),
                            'save_always' => true,
                            'dependency'  => array('element' => 'subtitle', 'not_empty' => true),
                            'group'       => esc_html__('Subtitle Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'textfield',
                            'param_name' => 'subtitle_margin',
                            'heading'    => esc_html__('Subtitle Top Margin (px)', 'sarto-core'),
                            'dependency' => array('element' => 'subtitle', 'not_empty' => true),
                            'group'      => esc_html__('Subtitle Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'textarea',
                            'param_name' => 'text',
                            'heading'    => esc_html__('Text', 'sarto-core')
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'text_tag',
                            'heading'     => esc_html__('Text Tag', 'sarto-core'),
                            'value'       => array_flip(sarto_edge_get_title_tag(true, array('p' => 'p'))),
                            'save_always' => true,
                            'dependency'  => array('element' => 'text', 'not_empty' => true),
                            'group'       => esc_html__('Text Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'colorpicker',
                            'param_name' => 'text_color',
                            'heading'    => esc_html__('Text Color', 'sarto-core'),
                            'dependency' => array('element' => 'text', 'not_empty' => true),
                            'group'      => esc_html__('Text Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'textfield',
                            'param_name' => 'text_font_size',
                            'heading'    => esc_html__('Text Font Size (px)', 'sarto-core'),
                            'dependency' => array('element' => 'text', 'not_empty' => true),
                            'group'      => esc_html__('Text Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'textfield',
                            'param_name' => 'text_line_height',
                            'heading'    => esc_html__('Text Line Height (px)', 'sarto-core'),
                            'dependency' => array('element' => 'text', 'not_empty' => true),
                            'group'      => esc_html__('Text Style', 'sarto-core')
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'text_font_weight',
                            'heading'     => esc_html__('Text Font Weight', 'sarto-core'),
                            'value'       => array_flip(sarto_edge_get_font_weight_array(true)),
                            'save_always' => true,
                            'dependency'  => array('element' => 'text', 'not_empty' => true),
                            'group'       => esc_html__('Text Style', 'sarto-core')
                        ),
                        array(
                            'type'       => 'textfield',
                            'param_name' => 'text_margin',
                            'heading'    => esc_html__('Text Top Margin (px)', 'sarto-core'),
                            'dependency' => array('element' => 'text', 'not_empty' => true),
                            'group'      => esc_html__('Text Style', 'sarto-core')
                        )
                    )
                )
            );
        }
    }

    public function render($atts, $content = null) {
        $args = array(
            'custom_class'         => '',
            'type'                 => 'standard',
            'title_position'       => 'title-left',
            'columns_space'        => 'normal',
            'position'             => '',
            'holder_padding'       => '',
            'title'                => '',
            'title_tag'            => 'h3',
            'title_color'          => '',
            'title_underscore'     => '',
            'title_underscore_size' => 'normal',
            'title_bold_words'     => '',
            'title_light_words'    => '',
            'title_break_words'    => '',
            'disable_break_words'  => '',
            'subtitle'             => '',
            'subtitle_tag'         => 'span',
            'subtitle_color'       => '',
            'subtitle_font_size'   => '',
            'subtitle_line_height' => '',
            'subtitle_font_weight' => '',
            'subtitle_margin'      => '',
            'text'                 => '',
            'text_tag'             => 'p',
            'text_color'           => '',
            'text_font_size'       => '',
            'text_line_height'     => '',
            'text_font_weight'     => '',
            'text_margin'          => ''
        );
        $params = shortcode_atts($args, $atts);

        $params['holder_classes'] = $this->getHolderClasses($params, $args);
        $params['holder_styles'] = $this->getHolderStyles($params);
        $params['title'] = $this->getModifiedTitle($params);
        $params['title_tag'] = !empty($params['title_tag']) ? $params['title_tag'] : $args['title_tag'];
        $params['title_styles'] = $this->getTitleStyles($params);
        $params['title_underscore_style'] = $this->getUnderscoreStyle($params);
        $params['subtitle_tag'] = !empty($params['subtitle_tag']) ? $params['subtitle_tag'] : $args['subtitle_tag'];
        $params['subtitle_styles'] = $this->getSubtitleStyles($params);
        $params['text_tag'] = !empty($params['text_tag']) ? $params['text_tag'] : $args['text_tag'];
        $params['text_styles'] = $this->getTextStyles($params);

        $html = sarto_core_get_shortcode_module_template_part('templates/section-title', 'section-title', '', $params);

        return $html;
    }

    private function getHolderClasses($params, $args) {
        $holderClasses = array();

        $holderClasses[] = !empty($params['custom_class']) ? esc_attr($params['custom_class']) : '';
        $holderClasses[] = !empty($params['type']) ? 'edgtf-st-' . $params['type'] : 'edgtf-st-' . $args['type'];
        $holderClasses[] = !empty($params['title_position']) ? 'edgtf-st-' . $params['title_position'] : 'edgtf-st-' . $args['title_position'];
        $holderClasses[] = !empty($params['columns_space']) ? 'edgtf-st-' . $params['columns_space'] . '-space' : 'edgtf-st-' . $args['columns_space'] . '-space';
        $holderClasses[] = $params['disable_break_words'] === 'yes' ? 'edgtf-st-disable-title-break' : '';
        $holderClasses[] = $params['title_underscore'] === 'yes' ? 'edgtf-st-title-underscore' : '';

        return implode(' ', $holderClasses);
    }

    private function getHolderStyles($params) {
        $styles = array();

        if (!empty($params['holder_padding'])) {
            $styles[] = 'padding: 0 ' . $params['holder_padding'];
        }

        if (!empty($params['position'])) {
            $styles[] = 'text-align: ' . $params['position'];
        }

        return implode(';', $styles);
    }

    private function getModifiedTitle($params) {
        $title = $params['title'];
        $title_bold_words = str_replace(' ', '', $params['title_bold_words']);
        $title_light_words = str_replace(' ', '', $params['title_light_words']);
        $title_break_words = str_replace(' ', '', $params['title_break_words']);

        if (!empty($title)) {
            $bold_words = explode(',', $title_bold_words);
            $light_words = explode(',', $title_light_words);
            $split_title = explode(' ', $title);

            if (!empty($title_bold_words)) {
                foreach ($bold_words as $value) {
                    if (!empty($split_title[$value - 1])) {
                        $split_title[$value - 1] = '<span class="edgtf-st-title-bold">' . $split_title[$value - 1] . '</span>';
                    }
                }
            }

            if (!empty($title_light_words)) {
                foreach ($light_words as $value) {
                    if (!empty($split_title[$value - 1])) {
                        $split_title[$value - 1] = '<span class="edgtf-st-title-light">' . $split_title[$value - 1] . '</span>';
                    }
                }
            }

            if (!empty($title_break_words)) {
                if (!empty($split_title[$title_break_words - 1])) {
                    $split_title[$title_break_words - 1] = $split_title[$title_break_words - 1] . '<br />';
                }
            }

            $title = implode(' ', $split_title);
        }

        return $title;
    }

    private function getTitleStyles($params) {
        $styles = array();

        if (!empty($params['title_color'])) {
            $styles[] = 'color: ' . $params['title_color'];
        }

        return implode(';', $styles);
    }

    private function getSubtitleStyles($params) {
        $styles = array();

        if (!empty($params['subtitle_color'])) {
            $styles[] = 'color: ' . $params['subtitle_color'];
        }

        if (!empty($params['subtitle_font_size'])) {
            $styles[] = 'font-size: ' . sarto_edge_filter_px($params['subtitle_font_size']) . 'px';
        }

        if (!empty($params['subtitle_line_height'])) {
            $styles[] = 'line-height: ' . sarto_edge_filter_px($params['subtitle_line_height']) . 'px';
        }

        if (!empty($params['subtitle_font_weight'])) {
            $styles[] = 'font-weight: ' . $params['subtitle_font_weight'];
        }

        if ($params['subtitle_margin'] !== '') {
            $styles[] = 'margin-top: ' . sarto_edge_filter_px($params['subtitle_margin']) . 'px';
        }

        return implode(';', $styles);
    }

    private function getTextStyles($params) {
        $styles = array();

        if (!empty($params['text_color'])) {
            $styles[] = 'color: ' . $params['text_color'];
        }

        if (!empty($params['text_font_size'])) {
            $styles[] = 'font-size: ' . sarto_edge_filter_px($params['text_font_size']) . 'px';
        }

        if (!empty($params['text_line_height'])) {
            $styles[] = 'line-height: ' . sarto_edge_filter_px($params['text_line_height']) . 'px';
        }

        if (!empty($params['text_font_weight'])) {
            $styles[] = 'font-weight: ' . $params['text_font_weight'];
        }

        if ($params['text_margin'] !== '') {
            $styles[] = 'margin-top: ' . sarto_edge_filter_px($params['text_margin']) . 'px';
        }

        return implode(';', $styles);
    }


	private function getUnderscoreStyle($params) {
		$styles = array();

		switch($params['title_underscore_size']) {
			case 'large' : {
				$styles['width'] = 218;
				$styles['height'] = 24;
			} break;
			case 'normal' : {
				$styles['width'] = 109;
				$styles['height'] = 12;
			} break;
			default : {
				$styles['width'] = 109;
				$styles['height'] = 12;
			} break;
		}

		return $styles;
	}
}