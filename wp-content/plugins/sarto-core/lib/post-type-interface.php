<?php

namespace SartoCore\Lib;

/**
 * interface PostTypeInterface
 * @package SartoCore\Lib;
 */
interface PostTypeInterface {
	/**
	 * @return string
	 */
	public function getBase();
	
	/**
	 * Registers custom post type with WordPress
	 */
	public function register();
}