<?php
/*
Plugin Name: Sarto Twitter Feed
Description: Plugin that adds Twitter feed functionality to our theme
Author: Edge Themes
Version: 1.0
*/

include_once 'load.php';

if ( ! function_exists( 'sarto_twitter_theme_installed' ) ) {
	/**
	 * Checks whether theme is installed or not
	 * @return bool
	 */
	function sarto_twitter_theme_installed() {
		return defined( 'EDGE_ROOT' );
	}
}

if ( ! function_exists( 'sarto_twitter_feed_text_domain' ) ) {
	/**
	 * Loads plugin text domain so it can be used in translation
	 */
	function sarto_twitter_feed_text_domain() {
		load_plugin_textdomain( 'sarto-twitter-feed', false, SARTO_TWITTER_REL_PATH . '/languages' );
	}
	
	add_action( 'plugins_loaded', 'sarto_twitter_feed_text_domain' );
}